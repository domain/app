// @ts-check

/**
  Manages pre-warmed Kitten servers. 
*/
import crypto from 'node:crypto'

import { DNSimple } from 'dnsimple'
import HetznerCloud from 'hcloud-js'
import { kitten, events, _db } from '@small-web/kitten'

import db from '@app/database'
import cloudInit from './settings🔒/places/create/cloudInit.script.js' 
import deleteDomain from './settings🔒/lib/deleteDomain.script.js'

const eventNameToTitle = eventName => eventName[0].toUpperCase() + eventName.replace(/-/g, ' ').slice(1) + '.'

async function wait (seconds) {
  return new Promise((resolve, reject) => setTimeout (() => resolve(true), seconds*1000))
}

export default class PreWarmedDomains {
  static isBeingInstantiatedByStaticAccessor = false
  /** @type {PreWarmedDomains|null} */
  static instance = null

  numberOfDomainCreationErrors = 0
  numberOfDomainDeletionErrors = 0
  okToContinueAttemptingToCreateDomains = true
  okToContinueAttemptingToDeleteDomains = true

  MIN_NUMBER_OF_PRE_WARMED_DOMAINS = 0
  MAX_NUMBER_OF_PRE_WARMED_DOMAINS = 10
  MAX_NUMBER_OF_DOMAIN_CREATION_ATTEMPTS = 3
  MAX_NUMBER_OF_DOMAIN_DELETION_ATTEMPTS = 3

  /**
    @returns {PreWarmedDomains}
  */
  static getInstance () {
    if (this.instance === null) {
      this.isBeingInstantiatedByStaticAccessor = true
      this.instance = new PreWarmedDomains()
      this.isBeingInstantiatedByStaticAccessor = false
    }
    return this.instance
  }

  constructor () {
    if (!PreWarmedDomains.isBeingInstantiatedByStaticAccessor) {
      throw new Error('🔥 PreWarmedDomains is a singleton. Please use its getInstance() method to obtain a reference.')
    }
  }

  /**
    Returns the next available prewarmed domain or null if there isn’t one.
    @returns {import('@app/database').Domain?}
  */
  getNextAvailablePreWarmedDomain () {
    if (!db.settings.ok) {
      console.warn('🔥 Pre-warmed domains not currently available as Domain setup is not complete.')
      return null
    }
    const nextAvailablePreWarmedSubdomain = db.preWarmedDomains.pop()

    if (nextAvailablePreWarmedSubdomain === undefined) {
      return null
    } else {
      // Start creating another pre-warmed domain in its place.
      this.createOrDeletePreWarmedDomainsAsNecessary()

      const nextAvailablePreWarmedDomain = db.domains[nextAvailablePreWarmedSubdomain]
      return nextAvailablePreWarmedDomain
    }
  }

  /**
    Updates the number of requested pre-warmed domains.

    @param {number} newNumberOfRequestedPreWarmedDomains
  */
  updateNumberOfRequestedPreWarmedDomains (newNumberOfRequestedPreWarmedDomains) {
    const currentNumberOfPreWarmedDomains = db.settings.vps.numberOfPreWarmedDomains

    if (currentNumberOfPreWarmedDomains === newNumberOfRequestedPreWarmedDomains) {
      // Same number as before, ignore.
      return
    }

    if (newNumberOfRequestedPreWarmedDomains < this.MIN_NUMBER_OF_PRE_WARMED_DOMAINS || newNumberOfRequestedPreWarmedDomains > this.MAX_NUMBER_OF_PRE_WARMED_DOMAINS) {
      console.warn(`🔥 Requested number of pre-warmed domains (${newNumberOfRequestedPreWarmedDomains}) is not within allowed limits (between ${this.MIN_NUMBER_OF_PRE_WARMED_DOMAINS} and ${this.MAX_NUMBER_OF_PRE_WARMED_DOMAINS}). Ignoring request.`)
      return
    }

    db.settings.vps.numberOfPreWarmedDomains = newNumberOfRequestedPreWarmedDomains

    console.info(`🔥 Updated number of pre-warmed domains to ${newNumberOfRequestedPreWarmedDomains}.`)
    this.createOrDeletePreWarmedDomainsAsNecessary()
  }

  /**
    Delete failed pre-warmed domains, if any.
  */
  async deleteFailedPreWarmedDomainsIfAny () {
    console.info('\n🔥 Checking for pre-warmed domains that failed during setup.')
    let numberOfFailedPreWarmedDomains = 0
    let numberOfUnhandledIssues = 0
    // Use for…of loop instead of forEach so async/await works inside it.
    for (const [subdomain, domain] of Object.entries(db.domains)) {
      if (domain.setup.complete === false || domain.setup.hasError === true) {
        numberOfFailedPreWarmedDomains++
        console.warn(`🔥 Warning: Found pre-warmed domain with failed setup (${subdomain}). Deleting it (including its DNS and VPS resources, etc.)`)
        try {
          await deleteDomain(subdomain)
        } catch (error) {
          console.error(`🔥 Error: Could not delete pre-warmed domain (${subdomain}). Please carry out manual housekeeping if necessary.`, error)
          numberOfUnhandledIssues++
        }
      }
    }
    if (numberOfFailedPreWarmedDomains > 0) {
      console.warn(`🔥 Warning: There were ${numberOfFailedPreWarmedDomains} failed pre-warmed domain${numberOfFailedPreWarmedDomains > 1 ? 's' : ''}, ${numberOfUnhandledIssues > 0 ? `${numberOfFailedPreWarmedDomains} of which we had problem deleting. You might want to check your VPS and DNS providers and carry out manual housekeeping if necessary.` : 'all of which have now been succesfully removed.'}`)
    }
  }

  /**
    Create a pre-warmed domain, if necessary, then check again.
  */
  async createOrDeletePreWarmedDomainsAsNecessary () {
    if (!db.settings.ok) {
      console.warn('🔥 Pre-warmed domains not currently available as Domain setup is not complete.')
      return false
    }

    // First prune any failed pre-warmed domains that might exist.
    // (This should occur infrequently in production but can easily happen
    // during development when the server restarts in the middle of pre-warmed
    // domain creation due to live reload.)
    await this.deleteFailedPreWarmedDomainsIfAny()

    console.log('\n🔥 Creating or deleting pre-warmed domains as necessary.') 
    const requestedNumberOfPreWarmedDomains = db.settings.vps.numberOfPreWarmedDomains
    const actualNumberOfPreWarmedDomains = db.preWarmedDomains.length

    if (actualNumberOfPreWarmedDomains < requestedNumberOfPreWarmedDomains) {
      // We have too few pre-warmed domains, create a new one.
      try {
        // FIXME: This `await(await…` is a smell.
        await(await this.createPreWarmedDomain())
      } catch (error) {
        console.error(error)
        // Ensure that we don’t just keep retrying if there are consistent errors in
        // creating new domains.
        this.numberOfDomainCreationErrors++
        if (this.numberOfDomainCreationErrors === this.MAX_NUMBER_OF_DOMAIN_CREATION_ATTEMPTS) {
          this.okToContinueAttemptingToCreateDomains = false
          console.warn('🔥 Maximum number of pre-warmed domain creation attempt errors reached. Not retrying.')
        }
        // TODO: Also save this error state in the database so we can surface it in the Settings interface.
      }
      // Let’s check again so we keep going until we get the number we want.
      if (this.okToContinueAttemptingToCreateDomains) {
        this.createOrDeletePreWarmedDomainsAsNecessary()
      }
    } else if (actualNumberOfPreWarmedDomains > requestedNumberOfPreWarmedDomains) {
      // We have too many pre-warmed domains, delete one.
      try {
        await this.deletePreWarmedDomain()
      } catch (error) {
        console.error(error)
        this.numberOfDomainDeletionErrors++
        if (this.numberOfDomainDeletionErrors === this.MAX_NUMBER_OF_DOMAIN_DELETION_ATTEMPTS) {
          this.okToContinueAttemptingToDeleteDomains = false
          console.warn('🔥 Maximum number of pre-warmed domain deletion attempt errors reached. Not retrying.')
        }
      }
      if (this.okToContinueAttemptingToDeleteDomains) {
        this.createOrDeletePreWarmedDomainsAsNecessary()
      }
    } else {
      // OK, we have the right number of pre-warmed domains.
      console.info(`🔥 All ${requestedNumberOfPreWarmedDomains} pre-warmed domains exist. Nothing to do.`)
      return
    }
  }

  /**
    Deletes an existing pre-warmed domain.
  */
  async deletePreWarmedDomain () {
    const subdomain = db.preWarmedDomains.pop()
    if (subdomain !== undefined) {
      console.info(`🔥 Deleting pre-warmed domain at subdomain ${subdomain}.`)
      try {
        await deleteDomain(subdomain)
      } catch (error) {
        // Could not delete the domain, push it back on the list.
        db.preWarmedDomains.push(subdomain)
        // @ts-ignore Error with cause (unsupported by TypeScript at the moment).
        throw new Error('🔥 Pre-warmed domain deletion failed.', { cause: error })
      }
    } else {
      throw new Error('🔥 Cannot find pre-warmed domain to delete.')
    }
  }
  
  /**
    Creates a new pre-warmed domain.
  */
  async createPreWarmedDomain () {
    return new Promise(async (resolve, reject) => {
      // We’ll create cryptographically unique subdomains. This isn’t
      // really a security feature but should help avoid DNS-related issues
      // when re-routing domain names. Script kiddies watching DNS records
      // will still be able to see new domains being created so it is not
      // a deterrant to DoS attacks or anything.
      const subdomain = crypto.randomUUID()

      // This is a minimal Kitten app with an index route that simply returns
      // a 200 OK response to a GET request (so we know the deployment worked).
      const appGitHttpsUrl = 'https://codeberg.org/domain/toasty-kitten.git'
    
      // Create a domain host token the Small Web server can use to communicate with
      // the domain host and save it in the database. When a status update is received from 
      // the server being set up, we inform the person in the interface of the progress.
      //
      // For example, if the domain is aral and the domain token is X and the
      // domain is small-web.org, aral.small-web.org will make a socket connection to
      // wss://small-web.org/domain/X/ and will send the following statuses: 
      // following statuses: configuring-server, installing-kitten, deploying-app.
      // After that, Kitten server itself will send a ready event when the server is up and running.
      const token = crypto.randomBytes(32).toString('hex')
    
      // Now that we have the secret token, we can also calculate the secret
      // one-time-use URL on the place being created where the person can set up the
      // secret/identity for their place.
      const secretUrl = `https://${subdomain}.${db.settings.dns.domain}/💕/hello/${token}/`

      console.info(`🔥 Creating new pre-warmed domain at subdomain ${subdomain}`)

      ////////////////////////////////////////////////////////////////////////////////
      //
      // Start place creation process.
      //
      ////////////////////////////////////////////////////////////////////////////////

      // Register this subdomain in the domains table if doesn’t already exist.
      // (This should never happen as server subdomains are cryptographically unique and
      // we should be in total control of server creation order and pre-warmed servers are
      // created one at a time. That said, shit happens.)
      if (db.domains[subdomain] !== undefined) {
        reject(`${subdomain} already registered or in the process of being registered (this should never happen).`)
      }

      db.domains[subdomain] = {
        subdomain,
        domain: db.settings.dns.domain,
        isPreWarmed: true,
        setup: {
          complete: false,
          hasError: false,
          cancelled: false,
          state: 'registration-started',
          startedAt: new Date()
        },
        server: {
          token
        },
        application: {
          // We’re installing a very basic placeholder Kitten app. We could special-case
          // this and build it into Kitten but the fewer moving parts, the better.
          // (Although this is definitely an optimisation we could consider in the future.)
          name: 'Toasty Kitten',
          source: appGitHttpsUrl
        }
      }
      const domainRecord = db.domains[subdomain]

      domainRecord.setup.state = 'about-to-create-cloud-init-configuration'

      ////////////////////////////////////////////////////////////////////////////////
      //
      // Create cloud-init configuration.
      //
      ////////////////////////////////////////////////////////////////////////////////

      const cloudInitConfiguration = cloudInit({
        token,
        subdomain,
        domain: db.settings.dns.domain,
        // This is the actual domain that the Domain instance is running on
        // (versus the one where the Small Web place is being set up). This
        // separation is important for testing locally via a staging server
        // but also, in the future, if we want to register places at domains
        // other than the one Domain is running on (on any TLD, for example).
        // TODO: For that to happen, we also have to handle empty subdomains
        // for setting up places at root domains.
        callbackDomain: kitten.domain,
        appGitHttpsUrl,
        sshKey: _db.settings.id.ssh.asString
      })

      ////////////////////////////////////////////////////////////////////////////////
      //
      // Provision web server.
      //
      ////////////////////////////////////////////////////////////////////////////////

      // Build the server. This is currently hardcoded to use Hetzner.
      // In the future, the goal is to have multiple vendors and transparently
      // switch between them.

      // Create server and store returned IP address.
      //
      // Note: while the Hetzner API documentation states that the
      // ===== sshKey provided in the call is optional, it is not.
      //       If not provided, a root password is set (which we don’t want)
      //       and the sudo account creation fails during cloud-config/cloud-init.
      //       So the less-than-ideal scenario is the we use the Domain host’s
      //       ssh key, which Kitten will then replace with the person’s own ssh key.
      //       (This also means that if something goes wrong during account creation
      //       the Domain host can connect via ssh and try to diagnose the problem.)
      const webHost = new HetznerCloud.Client({
        token: db.settings.vps.apiToken,
        timeout: 30000 // 30 seconds
      })

      domainRecord.setup.state = 'about-to-provision-server'

      let lastUpdateTime = new Date()

      const serverSetupStatusHandler = async status => {
        // Easiest way to avoid scope issues to bind to an object that has
        // the delete server method.
        const currentTime = new Date()
        let timeUnit = 'ms'
        let timeSinceLastUpdate = Number(lastUpdateTime) - Number(currentTime)
        lastUpdateTime = currentTime

        if (timeSinceLastUpdate > 1000) {
          timeSinceLastUpdate = timeSinceLastUpdate/1000
          timeUnit = 'seconds'
        }

        console.info(`+${timeSinceLastUpdate} ${timeUnit}: ${eventNameToTitle(status)}`)
        domainRecord.setup.state = status

        if (status === 'server-ready') {
          // This is the last status we expect from the CloudInit script. We
          // can remove the listener now so we don’t leak memory.
          events.removeListener(token, serverSetupStatusHandler)

          // Wait five seconds to give it lots of time to stabilise.
          await wait(5)

          // The server is ready so let’s make the first request (to trigger
          // the Let’s Encrypt TLS certificate privisioning). Once this
          // call successfully returns, we can be sure that the server can
          // be accessed immediately by the person so we can tell them that
          // it’s ready.
          const url = `https://${subdomain}.${db.settings.dns.domain}`

          let response
          try {
            response = await fetch(url)
          } catch (error) {
            // Could not fetch page. I don’t think we can recover from this.
            console.error(error)
            domainRecord.setup.endedAt = new Date()
            domainRecord.setup.hasError = true
            domainRecord.setup.error = 'Fetch filed while attempting to load pre-warmed domain.'
            domainRecord.setup.state = 'fetch-failed-while-attempting-to-load-pre-warmed-domain'
            reject(domainRecord.setup.error)
            return
          }

          domainRecord.setup.endedAt = new Date()
          if (response.ok) {
            domainRecord.setup.state = 'place-is-reachable'
            domainRecord.setup.complete = true
            db.preWarmedDomains.push(subdomain)
            resolve(domainRecord)
            return
          } else {
            domainRecord.setup.hasError = true
            domainRecord.setup.error = `${response.status} ${response.statusText}: ${await response.text()}`
            domainRecord.setup.state = 'failed-while-attempting-to-reach-deployed-place'
            reject('Failed while attempting to reach deployed place')
            return
          }
        }
      }

      events.addListener(token, serverSetupStatusHandler)

      let serverBuildResult
      try {
        serverBuildResult = webHost.servers.build()
        .name(`${subdomain}.${db.settings.dns.domain}`)
        .serverType(db.settings.vps.serverTypeName)
        .location(db.settings.vps.locationName)
        .image(db.settings.vps.imageName)
        .userData(cloudInitConfiguration)

        const sshKeyName = db.settings.vps.sshKeyName
        if (sshKeyName !== 'None (recommended)') {
          serverBuildResult = serverBuildResult.sshKey(db.settings.vps.sshKeyName)
        }
        
        serverBuildResult = await serverBuildResult.create()
      } catch (error) {
        console.error('VPS error', error)
        events.removeListener(token, serverSetupStatusHandler)
        domainRecord.setup.endedAt = new Date()
        domainRecord.setup.hasError = true
        domainRecord.setup.error = error.toString()
        domainRecord.setup.state = 'failed-while-creating-vps-server'

        /** @ts-ignore Error with cause. */
        throw new Error('VPS error', { cause: error })
      }

      // Persist server details from build result.
      domainRecord.server.id = serverBuildResult.server.id
      domainRecord.server.ipv4 = serverBuildResult.server.publicNet.ipv4.ip
      domainRecord.server.ipv6 = serverBuildResult.server.publicNet.ipv6.ip
      domainRecord.server.createdAt = serverBuildResult.server.created

      // TODO: Check that the response is exactly as we expect it to be.
      // (The shape of the .server and .action properties.)

      ////////////////////////////////////////////////////////////////////////////////
      //
      // Set up DNS.
      //
      ////////////////////////////////////////////////////////////////////////////////

      const dnsHost = new DNSimple({
        accessToken: db.settings.dns.accessToken
      })

      const ipv4 = domainRecord.server.ipv4
      const _ipv6 = domainRecord.server.ipv6 // currently unused. TODO: look into this.

      // Create an A record for the subdomain that points to the server’s IP address.
      // TODO: ALso create AAAA record for the ipv6
      // Create zone record
      let dnsZoneCreationResponse
      try {
        // @ts-ignore DNSimple’s API as it’s not strongly typed.
        dnsZoneCreationResponse = await dnsHost.zones.createZoneRecord(
          /* accountId */ db.settings.dns.accountId,
          /* zoneId */ db.settings.dns.domain,
          /* attributes */ {name: subdomain, type: 'A', content: ipv4, ttl: 60}
        )
      } catch (error) {
        console.error('DNS error', error)
        events.removeListener(token, serverSetupStatusHandler)
        domainRecord.setup.endedAt = new Date()
        domainRecord.setup.hasError = true
        domainRecord.setup.error = error.message
        domainRecord.setup.state = 'failed-while-setting-up-dns'

        /** @ts-ignore Error with cause. */
        throw new Error('DNS error', { cause: error })
      }

      // Persist zone record id (so we can use it to delete it if we need to later).
      domainRecord.dns = {
        id: dnsZoneCreationResponse.data.id
      }

      domainRecord.setup.state = 'domain-created'
    })
  }
}
