import { html } from '@small-web/kitten'
import Styles from './Navigation.fragment.css'

const pages = [
  {id: 'join', link: '/', title: 'Join!'},
  {id: 'about', link: '/about', title: 'About'},
  {id: 'faq', link: '/faq', title: 'FAQ'}
]

export default ({pageId}) => {
  return html`
    <nav>
      <ul>
        ${pages.map(page => html`
          <if ${pageId === page.id}>
            <then>
              <page title='Kitten: ${page.title}'>
              <li class='selected'>${page.title}</li>
            </then>
            <else>
              <li><a href='${page.link}'>${page.title}</a></li>
            </else>
          </if>
        `)}
        <li><a href='https://small-tech.org/fund-us/' target='_blank'>Fund the Small Web</a></li>
      </ul>
    </nav>
    <${Styles} />
  `
}
