// @ts-check

import Application from './Application.layout.js'
import Navigation from './Navigation.fragment.js'
import ok from './settings🔒/setup/ok.script.js'

import { db } from '@app/database'
import { html, ALPINEJS, PAGE } from '@small-web/kitten'

export default function () {
  const paymentIsNone = db.settings.payment.selectedProvider === 'none'
  const paymentIsToken = db.settings.payment.selectedProvider === 'token'
  const paymentIsPaddle = db.settings.payment.selectedProvider === 'paddle'

  // Hard-coded for now.
  const paymentMode = 'test'

  const setupIsOk = ok()

  const paddleEnvironment = db.settings.payment.providers.paddle.environment
  const currentPaddleEnvironment = paddleEnvironment[paddleEnvironment.current]
  const paddlePlanId = currentPaddleEnvironment.planId
  const paddleVendorId = currentPaddleEnvironment.vendorId

  // Special case for small-web.org for the moment so I can deploy it
  // before it’s ready.
  let prerelease = false
  if (db.settings.dns.domain === 'small-web.org') {
    prerelease = true
  }

  const applications = db.settings.applications.list.concat(
    [
      {
        name: 'app',
        source: 'custom'
      }
    ]
  )

  // TODO: for testing; remove when done.
  prerelease = false

  return html`
    <page css alpinejs>
    <${Application}>
      <content for=${PAGE.head}>
        <script src='https://cdn.paddle.com/paddle/paddle.js'></script>
          <if ${paddleEnvironment.current === 'sandbox'}>
            <script>
              Paddle.Environment.set('sandbox')
            </script>
          </if>
        <script>
        	Paddle.Setup({ vendor: ${paddleVendorId} })
        </script>
      </>
  
      <header>
        <img class='domainLogo' src='/domain-logo.svg' alt='Domain logo: kitten sleeping by a tiny home while holding onto its yarn.'>
        <${Navigation} />
        <h1>Join the Small Web!</h1>
        <p>Get your own <a href='https://codeberg.org/place/app'>Place</a> on ${db.settings.dns.domain} ${paymentIsPaddle && `for €10/month`} in the next minute.</p>
      </header>

      <div id='javascriptRequired' x-init='$el.hidden = true'>
        <h2>Please turn on JavaScript to use this site.</h2>
        <p>Small Web sites require JavaScript to function. This is so we can protect your privacy by generating your secret in your browser so we never know what it is.</p>
      </div>
  
      <main hidden x-init='$el.hidden = false'>
        <if ${!setupIsOk}>
          <then>
            <!-- Setup is not OK. -->
            <h2>Set up required</h2>
            <p>Your instance is not properly configured yet.</p>
            <p>Please visit <a href='/settings'>settings</a> to finish setting it up.</p>
          <else>
            <!-- Setup is OK. -->
              <if ${paymentIsNone || prerelease}>
                <then>
                  <!-- Private instance. -->
                  <aside>
                    <if ${prerelease}>
                      <then>
                        <p><strong>Coming in 2024.</strong></p>
                        <p>
                          The world’s first <a href='https://ar.al/2020/08/07/what-is-the-small-web/'>Small Web</a> host, run by <a href='https://small-tech.org'>Small Technology Foundation</a>, will be launching this year.
                        </p>
                      </then>
                      <else>
                        <p><strong>This is a private instance.</strong></p>
                        <p>
                          Please <a href='mailto:${db.settings.organisation.email}'>contact your administrator</a> for help in setting up your own place or use a public domain like <a href='https://${db.settings.dns.domain}'>${db.settings.dns.domain}</a>.
                        </p>
                      </else>
                    </if>
                  </aside>
                </then>
                <else>
                  <div>
                    <form>
                      <div class='picker'>
                        <label id='domainLabel'>Pick a name</label>
                        <input id='domain' type='text' placeholder='Enter a name to see if it’s available'></input>
                        <div class='terms'>
                          <input id='terms' name='terms' type='checkbox'>
                          <label for='terms'>I agree to the <a href='/terms-and-conditions'>terms and conditions</a>.</label> 
                        </div>
                      </div>
                      <if ${!paymentIsNone}>
                        <div>
                          <button
                            type='submit'
                            @click.prevent='Paddle.Checkout.open({
                              product: ${paddlePlanId},
                              message: "Monthly charge for " + document.getElementById("domain").innerText + "." +  "${db.settings.dns.domain}"
                            })'
                          >Get started!</button>
                        </div>
                      </if>
                      <!--
                      <div class='appDetails'>
                        <h2>Place</h2>
                        <p>Place is a personal social network app. Make public posts and talk privately to other people on the Small Web.</p>
                        <p><a href='https://codeberg.org/place/app'>Learn more</a></p>
                      </div>
                      -->
                    </form>
                    <p class='advanced'><em><strong>Technically savvy?</strong> See <a href=''>advanced options</a>.</em></p>
                  </div>
                </else>
              </if>
              <!-- TODO: Make this configurable; do not hardcode. -->
              <!--
              <if ${!paymentIsNone}>
                <div>
                  <h2>Need help?</h2>
                  <p><strong>Need help?</strong> Email Laura and Aral at <a href='mailto:${db.settings.organisation.email}'>${db.settings.organisation.email}.</a></p>
                </div>
              </if>
              -->
        </if>
      </main>
    </>

    <style>
      header {
      }

      header img {
        display: block;
        margin: 0 auto;
      }

      header h1 {
        font-size: 4em;
        line-height: 1;
        font-weight: 600;
        margin: 0.5em 0 0 0;
        color: var(--accessible-deep-pink);
      }

      header .domainLogo {
        max-width: 14em;
      }

      header p {
        font-size: 1.5em;
        margin-top: 0.25em;
      }

      .Footer {
        margin-top: 4em;
        padding: 1em 0 1em 0.25em;
        text-style: normal;
      }

      .advanced {
        font-size: 1rem;
        margin-top: 2em;
      }

      .test-mode {
        width: 100%;
        padding: 0.5em;
        background-color: var(--accessible-deep-pink);
        color: white;
        font-weight: 600;
      }

      /* TODO: Copied from settings🔒/Settings.fragment.css; refactor. */
      form {
        font-size: 1.25em;
        background-color: var(--background-alt);
        border-radius: 0.5em;
        margin-left: -0.5em;
        margin-right: -0.5em;
        padding: 0.5em;
      }

      form label, form input, form button, form select, form textarea, form p {
        display: block;
      }


      form label {
        display:block;
        font-size: 1.25em;
        font-weight: 600;
        margin-bottom: 0.25em;
      }

      #domain {
        width: 320px;
      }

      .terms {
        display: flex;
        align-items: end;
        margin-top: 1rem;
      }

      input[type='checkbox'] {
        accent-color: var(--accessible-deep-pink)
      }

      .terms label {
        font-size: 1rem;
      }

    /*
      form input, form select, form textarea {
        margin: 0.5em 0 1.5em 0;
        width: 100%;
        padding: 0.25em;
      }
    */

    form button {
      margin-top: 1em;
    }

    .appDetails {
      background-color: var(--background-alt);
      border-radius: 1em;
      padding: 1em;
      font-size: 1rem;
      margin-top: 2em;
      margin-bottom: 0;
      margin-left: -1em;
      margin-right: -1em;
      padding-bottom: 0.5em;

      h2 {
        margin-top: 0;
      }
    }
    </style>
  `
}
