// @ts-check
import { html } from '@small-web/kitten'
import { db } from '@app/database'

import ok from './setup/ok.script.js'

// @ts-ignore CSS module.
import Styles from './Navigation.fragment.css'

export default function ({ activeSection }) {
  function isActive (section) {
    return section === activeSection.replace('&amp;', '&')  ? 'active' : ''
  }

  // Icon shapes courtesy of https://heroicons.com/ (MIT-licensed).
  const okIcon = ({ title }) => html`
    <svg class='icon ok' role='img' fill='none' viewBox='0 0 24 24' stroke-width='1.5' stroke='currentColor'>
      <title>${title} is set up correctly.</title>
      <path stroke-linecap='round' stroke-linejoin='round' d='M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z' />
    </svg>
  `

  const notOkIcon = ({ title }) => html`
    <svg class='icon not-ok' role='img' fill='none' viewBox='0 0 24 24' stroke-width='1.5' stroke='currentColor'>
      <title>${title} is NOT set up correctly.</title>
      <path stroke-linecap='round' stroke-linejoin='round' d='M12 9v3.75m9-.75a9 9 0 11-18 0 9 9 0 0118 0zm-9 3.75h.008v.008H12v-.008z' />
    </svg>
  `

  const needsInputIcon = ({ title }) => html`
    <svg class='icon needs-input' role='img' fill='none' viewBox='0 0 24 24' stroke-width='1.5' stroke='currentColor'>
      <title>${title} has not been set up yet.</title>
      <path stroke-linecap='round' stroke-linejoin='round' d='M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10' />
    </svg>
  `

  const lockedIcon = _ => html`
    <svg class='icon invisible' fill='none' viewBox='0 0 24 24' stroke-width='1.5' stroke='currentColor'>
      <title>Signed out: status not diplayed.</title>
      <path stroke-linecap="round" stroke-linejoin="round" d="M16.5 10.5V6.75a4.5 4.5 0 10-9 0v3.75m-.75 11.25h10.5a2.25 2.25 0 002.25-2.25v-6.75a2.25 2.25 0 00-2.25-2.25H6.75a2.25 2.25 0 00-2.25 2.25v6.75a2.25 2.25 0 002.25 2.25z" />
    </svg>
  `

  const signedOut = activeSection === 'Sign in'
  const setupOk = ok()
  const setupSectionKeys = ['organisation', 'applications', 'psl', 'dns', 'vps', 'payment']

  let numberOfSectionsWithErrors = 0
  let numberOfSectionsThatNeedInput = 0

  const setupSections = setupSectionKeys.map(key => {
    const setupSectionData = db.settings[key]
    const title = setupSectionData.title
    const link = `/settings/setup/${key}`

    let icon
    switch(true) {
      case (signedOut):
        icon = lockedIcon()
        break
      case (!signedOut && setupSectionData.ok):
        icon = okIcon({title})
        break
      case (!signedOut && setupSectionData.setupAttempted && !setupSectionData.ok):
        icon = notOkIcon({title})
        numberOfSectionsWithErrors++
        break
      case (!signedOut && !setupSectionData.ok && !setupSectionData.setupAttempted):
        icon = needsInputIcon({title})
        numberOfSectionsThatNeedInput++
        break
      default:
        // This should not happen.
        console.warn(`Warning: encountered unexpected icon state on ${setupSectionData}.`)
    }

    return { title, icon, link }
  })

  // FIXME: Since this is an autheticated route, we don’t need the checks for
  // ===== sign-in status as a signed out person would never reach this page.
  const component = html`
    <nav class='Navigation'>
      <header>
        <a href='/'><img class='domainLogo' src='/domain-logo.svg' alt='Domain logo: kitten sleeping by a tiny home while holding onto its yarn.'></a>
        <div id='skip'>
          <a href='#content'>Skip to main content.</a>
        </div>
      </header>
      <ul>
        <li>
          <section>
            <h1>Settings</h1>
            <ul class='sub-nav'>
              <if ${signedOut}>
                <then><li><a class=${isActive('Sign in')} href='/💕/sign-in'>Sign in</a></li>
                <else><li><a class=${isActive('Sign out')} href='/💕/sign-out'>Sign out</a></li>
              </if>
              <li>
                <a href='/🐱/settings'>🐱 Kitten Settings</a>
              </li>
            </ul>
          </section>
        </li>
        <li inert=${signedOut || !setupOk} aria-disabled=${signedOut || !setupOk ? "true" : "false"}>
          <section>
            <h2>Places</h2>
            <ul class='sub-nav'>
              <li><a class='${isActive('View')}' href='/settings/places/'>View</a></li>
              <li><a class='${isActive('Create')}' href='/settings/places/create/'>Create</a></li>
              <li><a class='${isActive('Import & Export')}' href='/settings/places/import-export/'>Import & Export</a></li>
              <li><a class='${isActive('Pre-warmed')}' href='/settings/places/pre-warmed/'>Pre-warmed</a></li>
              <li><a class='${isActive('Troubleshoot')}' href='/settings/places/troubleshoot/'>Troubleshoot</a></li>
            </ul>
          </section>
        </li>
        <li inert=${signedOut}>
          <section>
            <h2>Setup</h2>

            <if ${!signedOut && !setupOk}>
              <aside>
                <if ${!setupOk && numberOfSectionsWithErrors > 0}>
                  <p>
                    The ${numberOfSectionsWithErrors === 1 ? 'section' : 'sections'} marked with <${notOkIcon} title='Icon: '/> ${numberOfSectionsWithErrors === 1 ? 'is' : 'are' } misconfigured and ${numberOfSectionsWithErrors === 1 ? 'needs' : 'need'} your attention.
                  </p>
                </if>

                <if ${!setupOk && numberOfSectionsWithErrors === 0}>
                  <p>
                    The ${numberOfSectionsThatNeedInput === 1 ? 'section' : 'sections'} marked with <${needsInputIcon} title='Icon: '/> ${numberOfSectionsThatNeedInput === 1 ? 'is' : 'are' } not configured yet and ${numberOfSectionsThatNeedInput === 1 ? 'needs' : 'need'} your attention.
                  </p>
                </if>
              </aside>
            </if>

            <ul class='sub-nav'>
              ${setupSections.map(setupSection => {
                return html`
                  <li>
                    <a class=${`${isActive(setupSection.title)} icon`} href=${setupSection.link}>
                      ${[setupSection.icon]}
                      ${[setupSection.title]}
                    </a>
                  </li>
                `
              })}
            </ul>
          </section>
        </li>
      </ul>
      <${Styles} />
    </nav>
  `

  return component
}
