// @ts-check
import { db } from '@app/database'

import { redirect, unauthorised, getParameterObjectFromRequest  } from '../../../lib/utilities.script.js'

export default function ({ request, response }) {
  const applicationIndex = parseInt(request.params.applicationIndex)
  if (isNaN(applicationIndex)) {
    // Invalid application index.
    redirect('/settings/setup/applications')
    return
  }

  let parameters
  const requiredParameters = ['name', 'source']

  try {
    parameters = getParameterObjectFromRequest({ request, requiredParameters })
  } catch (error) {
    // If we’re being called by the front-end, these parameters
    // should exist. If they don’t it’s a security issue.
    console.error(error)
    return unauthorised(response)
  }

  db.settings.applications.setupAttempted = true

  // @ts-ignore Object that is runtime validated for correct form.
  db.settings.applications.list[applicationIndex] = parameters
  
  redirect('/settings/setup/applications', response)
}
