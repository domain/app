// @ts-check
import { db } from '@app/database'
import { HTMX, html } from '@small-web/kitten'

import ApplicationsAction from '../ApplicationsAction.layout.js'
import { redirect } from '../../../lib/utilities.script.js'

export default function ({ request, response }) {
  const applicationIndex = parseInt(request.params.applicationIndex)
  
  if (isNaN(applicationIndex)) {
    // The route has been called incorrectly.
    redirect('/settings/setup/applications/', response)
    return
  }
  
  const application = db.settings.applications.list[applicationIndex]

  if (application == undefined) {
    // The route has been called incorrectly.
    redirect('/settings/setup/applications/', response)
    return
  }

  return html`
    <${ApplicationsAction}>
      <form method='POST' action='/settings/setup/applications/edit/${applicationIndex}'>
        <h3>Edit application</h3>
        <label for='name'>Name</label>
        <input id='name' name='name' type='text' value='${application.name}' required>

        <label for='source'>Source (git repository https link)</label>
        <input id='source' name='source' type='url' value='${application.source}' required>

        <button id='add' name='add' type='submit'>Update</button>
      </form>
    </>
  `
}
