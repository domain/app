// @ts-check
import { html } from '@small-web/kitten'

// Applications layout extends Settings layout.
import Settings from '../../Settings.layout.js'

export default ({ SLOT, ...props }) => html`
  <${Settings} activeSection=Applications ...${props}>
    ${SLOT}
  </> 
`
