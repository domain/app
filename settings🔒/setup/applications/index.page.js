// @ts-check
import { db } from '@app/database'
import { HTMX, html } from '@small-web/kitten'

import Applications from './Applications.layout.js'

export default () => html`
  <${Applications}>
    <p>List of applications people can install from this instance.</p>
    <table>
      <thead>
        <tr>
          <th scope='col'>Name</th>
          <th scope='col'>Source</th>
          <th scope='col'>Actions</th>
        </tr>
      </thead>
      <tbody>
        ${
          db.settings.applications.list.map((application, index) => html`
            <tr>
              <td>${application.name}</td>
              <td><a href='${application.source}' target='_blank'>${application.source}</a></td>
              <td>
                <a class='action' href='/settings/setup/applications/edit/${index}'>Edit</a>
                <a class='action' href='/settings/setup/applications/delete/${index}'>Delete</a>
              </td>
            </tr>
          `)
        }
      </tbody>
    </table>
    <form method='POST' action='/settings/setup/applications'>
      <h3>Add application</h3>
      <label for='name'>Name</label>
      <input id='name' name='name' type='text' required />

      <label for='source'>Source (git repository https link)</label>
      <input id='source' name='source' type='url' required />

      <button id='add' name='add' type='submit'>Add</button>
    </form>
  </>
`
