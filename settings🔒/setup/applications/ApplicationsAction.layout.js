// @ts-check
import { html } from '@small-web/kitten'

import Applications from './Applications.layout.js'

export default ({ SLOT, ...props }) => html`
  <${Applications} ...${props}>
    <p><a href='/settings/setup/applications'>Back to applications list.</a></p>
    ${SLOT}
  </>
`