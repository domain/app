// @ts-check
import { db } from '@app/database'

import { redirect } from '../../../lib/utilities.script.js'

export default function ({ request, response }) {
  const applicationIndex = parseInt(request.params.applicationIndex)
  if (isNaN(applicationIndex)) {
    // Invalid application index.
    redirect('/settings/setup/applications')
    return
  }
  
  if (!request.body || !request.body.action) {
    // Invalid request.
    redirect('/settings/setup/applications/', response)
    return
  }
  
  if (request.body.action === 'confirm') {
    // Actually delete the item.
    const applicationToDelete = db.settings.applications.list[applicationIndex]
    if (applicationToDelete == undefined) {
      redirect('/settings/setup/applications/', response)
      return
    }
    db.settings.applications.list.splice(applicationIndex, 1)
  }
  
  // Update applications state.
  db.settings.applications.setupAttempted = true
  if (db.settings.applications.list.length === 0) {
    db.settings.applications.ok = false
  }

  redirect('/settings/setup/applications/', response)
}
