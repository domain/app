// @ts-check
import { db } from '@app/database'
import { HTMX, html } from '@small-web/kitten'

import ApplicationsAction from '../ApplicationsAction.layout.js'
import { redirect } from '../../../lib/utilities.script.js'

export default function ({ request, response }) {
  const applicationIndex = parseInt(request.params.applicationIndex)
  
  if (isNaN(applicationIndex)) {
    // The route has been called incorrectly.
    redirect('/settings/setup/applications/', response)
    return
  }
  
  const application = db.settings.applications.list[applicationIndex]

  if (application == undefined) {
    // The route has been called incorrectly.
    redirect('/settings/setup/applications/', response)
    return
  }

  return html`
    <${ApplicationsAction}>
      <form method='POST' action='/settings/setup/applications/delete/${applicationIndex}'>
        <h3>Delete application</h3>
        <p>Are you sure you want to delete ${application.name} (<a href='${application.source}'>${application.source}</a>)?</p>
        <div id='actions'>
          <button id='cancel' name='action' value='cancel' type='submit' default>No</button>
          <button id='confirm' name='action' value='confirm' type='submit'>Yes</button>
        </div>
      </form>
    </>

    <style>
      #actions {
        display: flex;
        column-gap: 1em;
      }
    </style>
  `
}
