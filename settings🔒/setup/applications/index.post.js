// @ts-check
import { db } from '@app/database'

import { getParameterObjectFromRequest, unauthorised, redirectToPage } from '../../lib/utilities.script.js'

export default function ({ request, response }) {
  const requiredParameters = ['name', 'source']
  let parameters 

  try {
    parameters = getParameterObjectFromRequest({ request, requiredParameters })
  } catch (error) {
    // If we’re being called by the front-end, these parameters
    // should exist. If they don’t it’s a security issue.
    console.error(error)
    return unauthorised(response)
  }

  db.settings.applications.setupAttempted = true

  // @ts-ignore Object that is runtime validated for correct form.
  db.settings.applications.list.push(parameters)

  db.settings.applications.ok = true

  redirectToPage({request, response})
}
