// @ts-check
import { db } from '@app/database'
import {  html } from '@small-web/kitten'

import Settings from '../../Settings.layout.js'

/**
  Organisation form.

*/
export default () => html`
  <${Settings} activeSection=Organisation>
    <p>These details are used to populate the legal matter in the privacy policy and terms and conditions, etc.</p>

    <form method='POST' action='/settings/setup/organisation'>
      <label for='name'>Name</label>
      <input
        id='name'
        name='name'
        type='text'
        required
        value=${db.settings.organisation.name}
        autocomplete='organization'
      >

      <label for='address'>Official Address</label>
      <textarea
        id='address'
        name='address'
        required
        autocomplete='street-address'
      >${db.settings.organisation.address}</textarea>

      <!-- ‘Hidden’ fields to make autocomplete work properly for the address field. -->
      <input class='hidden' type='text' tabindex='-1' name='addressLevel1' autocomplete='address-level1' placeholder='Address level 1' aria-hidden='true'/>
      <input class='hidden' type='text' tabindex='-1' name='addressLevel2' autocomplete='address-level2' placeholder='Address level 2' aria-hidden='true'/>
      <input class='hidden' type='text' tabindex='-1' name='addressLevel3' autocomplete='address-level3' placeholder='Address level 3' aria-hidden='true'/>
      <input class='hidden' type='text' tabindex='-1' name='addressLevel4' autocomplete='address-level4' placeholder='Address level 4' aria-hidden='true'/>
      <input class='hidden' type='text' tabindex='-1' name='postalCode' autocomplete='postal-code' placeholder='Postal code' aria-hidden='true'/>
      <input class='hidden' type='text' tabindex='-1' name='countryName' autocomplete='country-name' placeholder='Country' aria-hidden='true'/>

      <style>
        textarea { box-sizing: revert; }
        .hidden {
          position: absolute;
          z-index: -1;
          width: 1;
          height: 1;
          left: -100vw;
          top: -100vh;
        }
      </style>
      <script>
        ${[properAddressFieldAutoComplete.render()]}
      </script>

      <label for='site'>Web site</label>
      <input
        id='site'
        name='site'
        type='url'
        required
        autocomplete='url'
        value=${db.settings.organisation.site}
      >

      <label for='email'>Support email</label>
      <input
        id='email'
        name='email'
        type='email'
        required
        autocomplete='email'
        value=${db.settings.organisation.email}
      >

      <button type='submit'>Save</button>
    </form>
  </>
`

/**
  Client-side JavaScript to collate separate, hidden, address fields
  into the single visible one when autocomplete is triggered.

  Relevant docs: https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes/autocomplete
  Technique adapted from: https://codepen.io/Connum/pen/RwpeLar
*/
function properAddressFieldAutoComplete () {
  const inputs = ['addressLevel1', 'addressLevel2', 'addressLevel3', 'addressLevel4', 'postalCode', 'countryName']
  const inputNamed = (/** @type {string} */ name) => /** @type {HTMLInputElement} */ (document.querySelector(`[name="${name}"]`))
  const addressInput = inputNamed('address')
  const lastInput = inputNamed(inputs[inputs.length - 1])

  lastInput.addEventListener('input', () => {
    addressInput.value = inputs.reduce((fullAddress, inputName) => {
      const currentInputValue = inputNamed(inputName).value
      return fullAddress + (currentInputValue === '' ? '' : (', \n' + currentInputValue))
    }, addressInput.value) 
  })
}
properAddressFieldAutoComplete.render = () => properAddressFieldAutoComplete.toString().split('\n').slice(1, -1).join('\n')
