// @ts-check
import { db } from '@app/database'

import { getParameterObjectFromRequest, unauthorised, redirectToPage } from '../../lib/utilities.script.js'

export default function ({ request, response }) {
  const requiredParameters = ['name', 'address', 'site', 'email']
  let parameters

  try {
    parameters = getParameterObjectFromRequest({ request, requiredParameters })
  } catch (error) {
    // If we’re being called by the front-end, these parameters
    // should exist. If they don’t it’s a security issue.
    console.error(error)
    return unauthorised(response)
  }

  // Update the database record.
  requiredParameters.forEach(parameter => {
    db.settings.organisation[parameter] = parameters[parameter]
  })
  db.settings.organisation.setupAttempted = true
  
  redirectToPage({request, response})
}
