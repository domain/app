// @ts-check
import { db } from '@app/database'

async function fetchResource (method, args) {
  // Get server types. (In this first call we’ll know if the
  // authorisation token is correct or not.)
  let response
  try {
    response = await fetch(`https://api.hetzner.cloud/v1/${method}?per_page=50${args? args : ''}`, {
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${db.settings.vps.apiToken}`
      }
    })
  } catch (error) {
    // There was an error on our side while communicating with the VPS host.
    db.settings.vps.ok = false
    db.settings.vps.error = `Server error: could not connect to VPS API. Please try again later.`
    console.error(error.message, error.cause)
    return null
  }

  // VPS host API response received.

  if (response.status !== 200) {
    const error = `${response.status}: ${response.statusText}`
    db.settings.vps.ok = false
    db.settings.vps.error = error
    return null
  } else {
    db.settings.vps.ok = true
    db.settings.vps.error = ''
  }

  const responseJson = await response.json()

  if (responseJson.error) {
    db.settings.vps.ok = false
    db.settings.vps.error = `${responseJson.error.code}: ${responseJson.error.message}`
    return null
  }

  return responseJson[method]
}

export async function getServerTypes() {
  let serverTypes = await fetchResource('server_types')

  if (serverTypes !== null) {
    // Filter down to relevant server types
    serverTypes = serverTypes.filter(serverType => {
      // Flag the recommended server.
      serverType.description = `${serverType.name.toUpperCase()} (${serverType.cores} ${serverType.cpu_type === 'dedicated' ? 'dedicated' : 'shared'} ${serverType.architecture} cores, ${serverType.memory}GB memory, ${serverType.disk}GB disk)` 
      console.log(serverType.prices)
      if (serverType.name === 'cpx11') serverType.description += ' (recommended)'
      return Boolean(serverType.deprecated) === false
        && parseInt(serverType.cores) > 1
        && serverType.storage_type === 'local'
    })
  }

  return serverTypes
}

export async function getLocations() {
  let locations = await fetchResource('locations')

  // Mark servers in the US as not recommended for privacy reasons.
  locations = locations.map(location => {
    if (location.country === 'US') location.description += ' (privacy warning: US-based)'
    return location
  })

  return locations
}

export async function getImages() {
  let images = await fetchResource('images', '&type=system&status=available')

  images = images.map(image => {
    image.description += ` (${image.architecture})`
    if (image.name === 'alma-9') {
      image.description += ' (recommended)'
    }
    return image
  })
  
  // Return in reverse order so the versions display in descending order.
  return images.reverse()
}

export async function getSshKeys() {
  let sshKeys = await fetchResource('ssh_keys')
  
  // Add a None option (default) to the top of the SSH keys list.
  // By default, only the SSH key generated from the server-specific password
  // should be added to the server. The addition of any other SSH keys to the
  // server could be useful for private instances but will likely mean that
  // someone else has administrative access to your server on public instances
  // so we try to discourage that.
  sshKeys.unshift({
    id: -1,
    name: 'None (recommended)',
    fingerprint: 'None',
    public_key: 'None',
    labels: {},
    created: '0000-01-01T00:00:00+00:00'
  })
  
  return sshKeys
}

export async function getVpsDetails() {
  let atLeastOneCallFailed = false

  try {
    const [serverTypes, locations, images, sshKeys] = await Promise
    .all([getServerTypes(), getLocations(), getImages(), getSshKeys()])
    return { serverTypes, locations, images, sshKeys, numberOfPreWarmedDomains: -1}
  } catch (error) {
    console.error('Hetzner VPS API call failed', error)
    return null
  }
}
