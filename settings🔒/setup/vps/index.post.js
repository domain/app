// @ts-check
import { db } from '@app/database'

import { unauthorised, redirectToPage, getRequiredParametersFromRequest } from '../../lib/utilities.script.js'
import { getServerTypes } from './hetzner.script.js'

export default async function ({ request, response }) {
    const requiredParameters = ['apiToken']
  
    const { apiToken } = getRequiredParametersFromRequest(request, requiredParameters)
    if (apiToken === undefined) { return unauthorised(response) }

    db.settings.vps.apiToken = apiToken  

    // Flag that we have attempted to setup VPS (so it’s OK to show error messages, etc.
    // if the setup isn’t valid. We do this so we don’t show error messages before the
    // person has had a chance to attempt to set it up.)
    db.settings.vps.setupAttempted = true

    // Attempt to get server types. (This will show us if the API/bearer token works or not.)
    await getServerTypes()
    
    // See if it succeeded, and if so, update the selected server details also
    // (if they exist; they will not the first time the API Token is entered since
    // the form fields are not present on the page).
    if (db.settings.vps.ok && request.body.imageName !== undefined) {
      db.settings.vps.sshKeyName = request.body.sshKeyName
      db.settings.vps.serverTypeName = request.body.serverTypeName
      db.settings.vps.locationName = request.body.locationName
      db.settings.vps.imageName = request.body.imageName
      db.settings.vps.numberOfPreWarmedDomains = Number(request.body.numberOfPreWarmedDomains)
    }

    redirectToPage({request, response})
}
