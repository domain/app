// @ts-check
import { html } from '@small-web/kitten'
import { db } from '@app/database'

import Settings from '../../Settings.layout.js'
import ServiceState from '../ServiceState.script.js'
import { getVpsDetails } from './hetzner.script.js'

export default async function () {
  const state = new ServiceState()

  if (!db.settings.vps.setupAttempted) {
    state.set(state.states.UNKNOWN)
  } else if (db.settings.vps.ok) {
    const vpsDetails = await getVpsDetails() 
    if (vpsDetails === null) {
      state.set(state.states.UNKNOWN, { error: 'VPS API Error. Please retry.'})
    } else {
      vpsDetails.numberOfPreWarmedDomains = db.settings.vps.numberOfPreWarmedDomains
      state.set(state.states.OK, { vpsDetails })
    }
  } else {
    state.set(state.states.NOT_OK, { error: db.settings.vps.error })
  }

  return html`
    <page htmx>
    <${Settings} activeSection='Virtual Private Server (VPS)'>
      <h3>Hetzner</h3>

      <section class='instructions'>
        <h4>Instructions</h4>
        <ol>
          <li>Get a <a href='https://www.hetzner.com/cloud' target='_blank'>Hetzner Cloud</a> account.</li>
          <li><a href='https://accounts.hetzner.com/account/dpa' target='_blank'>Create a GDPR Data Protection Agreement</a>, accept it, download a copy, sign it, and keep it somewhere safe. (See <a href='https://docs.hetzner.com/general/general-terms-and-conditions/data-privacy-faq/' target='_blank'>Hetzner Data Privacy FAQ</a>)</li>
          <li><a href='https://console.hetzner.cloud/projects' target='_blank'>Create a new project</a> to hold the sites you will be hosting.</li>
          <li>Generate an API Token from <strong><em>your-project</em> → Security → API Tokens</strong> in your Hetzner dashboard and copy it below.</li>
        </ol>
      </section>

      <if ${!state.is(state.states.UNKNOWN)}>
        <if ${state.is(state.states.OK)}>
          <p class='correct'>Your VPS settings are correct.</p>
          <else>
            <if ${state.states.NOT_OK.error != null}>
              <p class='incorrect'>${state.states.NOT_OK.error}</p>
            </if>
        </if>
      <else>
        <if ${state.states.UNKNOWN.error != null}>
          <p class='incorrect'>${state.states.UNKNOWN.error}</p>
        </if>
      </if>

      <form method='POST' action='/settings/setup/vps'>
        <label for='apiToken'>API Token (with read/write permissions)</label>
        <input id='apiToken' name='apiToken' type='password' value=${(state.is(state.states.OK) || state.is(state.states.NOT_OK)) && db.settings.vps.apiToken}>

        <!-- If API key is correct, display VPS configuration. -->
        <if ${state.is(state.states.OK)}><then>
          <h3>Server details</h3>
          <p>These settings will be used when setting up servers.</p>

          <!-- Number of pre-warmed servers -->
          <label for='numberOfPreWarmedDomains'>Number of pre-warmed servers</label>
          <input
            id='numberOfPreWarmedDomains'
            name='numberOfPreWarmedDomains'
            type='number'
            min='1'
            max='10'
            value='${state.states.OK.vpsDetails?.numberOfPreWarmedDomains}'
          >

          <!-- VPS Server Types -->
          <label for='serverTypeName'>Server type</label>
          <select id='serverTypeName' name='serverTypeName'>
            ${state.states.OK.vpsDetails?.serverTypes?.map(serverType => (
              html`<option value=${serverType.name} selected=${serverType.name === db.settings.vps.serverTypeName}>${serverType.description}</option>`
            ))}
          </select>

          <!-- VPS Locations -->
          <label for='locationName'>Location</label>
          <select id='locationName' name='locationName'>
            ${state.states.OK.vpsDetails?.locations?.map(location => (
              html`<option value=${location.name} selected=${location.name === db.settings.vps.locationName}>${location.description.replace('DC', 'Data Centre')}</option>`
            ))}
          </select>

          <!-- VPS Images -->
          <label for='imageName'>Image</label>
          <select id='imageName' name='imageName'>
            ${state.states.OK.vpsDetails?.images?.map(image => (
              html`<option value=${image.name} selected=${image.name === db.settings.vps.imageName}>${image.description}</option>`
            ))}
          </select>
          <!-- SSH Keys -->
          <label for='sshKeyName'>Additional administrative SSH Key</label>
          <select id='sshKeyName' name='sshKeyName'>
            ${state.states.OK.vpsDetails?.sshKeys?.map(sshKey => (
              html`<option value=${sshKey.name} selected=${sshKey.name === db.settings.vps.sshKeyName}>${sshKey.name}</option>`
            ))}
          </select>

          <p class='vpsItemDetails'>If you select an SSH key, this will be added to the server <strong>in addition to the server-specific SSH key.</strong> Please understand the privacy implications of this. It means that whomever owns that SSH key will have administrative rights over this server. Please only use for private instances. On public instances, this will be publicly displayed on the sign-up page.</p>
        </if>

        <button type='submit'>Save</button>
      </form>
    </>
  `
}
