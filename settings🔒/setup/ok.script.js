// @ts-check
import { db } from '@app/database'

// Is setup OK?
export default ok => { 
  return db.settings.organisation.ok
    && db.settings.applications.ok
    && db.settings.psl.ok
    && db.settings.vps.ok
    && db.settings.payment.ok
}
