// @ts-check
import { html } from '@small-web/kitten'

import Settings from '../../Settings.layout.js'
import Provider from './Provider.fragment.js'

export default ({request}) => html`
  <${Settings} activeSection=Payment>
    <${Provider} session=${request.session} />
  </>
`
