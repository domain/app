/**
  Save/update Paddle settings.
*/

// @ts-check

import { db, PaddleEnvironment } from '@app/database'
import Form from './Form.fragment.js'
import { getParameterObjectFromRequest, unauthorised, redirect } from '../../../lib/utilities.script.js'

/** @typedef {{ vendorId: string, vendorAuthCode: string, planId: string }} PaddleEnvironmentObject */
/** @typedef {{ sandbox: PaddleEnvironmentObject, live: PaddleEnvironmentObject }} PaddleParameters */

const redirectUrl = '/settings/setup/payment/#payment-form'

export default async function ({ request, response }) {
  const requiredParameters = [
    'sandbox.vendorId', 'sandbox.vendorAuthCode', 'sandbox.planId',
    'live.vendorId', 'live.vendorAuthCode', 'live.planId'
  ]

  /** @type PaddleParameters */
  let parameters

  try {
    parameters = /** @type PaddleParameters */ (getParameterObjectFromRequest({ request, requiredParameters }))
  } catch (error) {
    // If we’re being called by the front-end, these parameters
    // should exist. If they don’t it’s a security issue.
    console.error(error)
    return unauthorised(response)
  }

  // Check if parameters are unchanged.
  const environmentIsUnchanged =
    (/** @type {'sandbox'|'live'} */ environmentId) => ['vendorId', 'vendorAuthCode', 'planId']
      .reduce(
        (state, parameter) =>
          state && parameters[environmentId][parameter] === db.settings.payment.providers.paddle.environment[environmentId][parameter]
        , true
      )

  if (environmentIsUnchanged(PaddleEnvironment.SANDBOX) && environmentIsUnchanged(PaddleEnvironment.LIVE)) {
    endRequestWithInformation ({
      request,
      response,
      information: 'Settings not updated as they were unchanged.'
    })
    return
  }

  // Update Paddle environments.
  await db.settings.payment.providers.paddle.environment.sandbox.update(parameters.sandbox)
  await db.settings.payment.providers.paddle.environment.live.update(parameters.live)
  
  endRequest({ request, response })
}

function endRequestWithInformation({ request, response, information }) {
  request.session.hasInformation = true
  request.session.information = information

  endRequest({ request, response })
}

function endRequest({ request, response }) {
  if (request.headers['hx-request']) {
    // Ajax call.
    response.end(Form({session: request.session}))
  } else {
    // Full page reload.
    redirect(redirectUrl, response)
  }
}
