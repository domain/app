// @ts-check

import { html } from '@small-web/kitten'
import { db } from '@app/database'

export default function ({ session }) {
  console.log('>', session)
  const environment = db.settings.payment.providers.paddle.environment
  const sandbox = environment.sandbox
  const live = environment.live

  let hasInformation = session.hasInformation
  let information = ''
  if (hasInformation) {
    information = session.information
    // Clear the information now that we’re about to display it.
    session.hasInformation = false
    session.information = ''
  }

  return html`
    <form
      id='paddle-form'
      method='POST'
      action='/settings/setup/payment/paddle/'
      hx-post='/settings/setup/payment/paddle/'
      hx-swap='outerHTML show:top'
    >
      <if ${hasInformation}>
        <p class='information'>${information}</p>
      </if>

      <section id='sandbox'>
        <h3>Sandbox environment</h3>

        <if ${sandbox.setupAttempted && !sandbox.ok}>
          <p class='incorrect'>${sandbox.error}</p>
        </if>

        <if ${sandbox.ok}>
          <p class='correct'>Sandbox environment settings are correct.</p>
        </if>

        <label for='sandbox-vendor-id'>Vendor ID <small><a href='https://sandbox-vendors.paddle.com/authentication' target='_blank'>(from here)</a></small></label>
        <input required id='sandbox-vendor-id' name='sandbox.vendorId' value='${sandbox.vendorId}' type='text'></input>

        <label for='sandbox-vendor-auth-code'>Vendor Auth Code <small>(secret API Key; <a href='https://sandbox-vendors.paddle.com/authentication' target='_blank'>from here</a>)</small></label>
        <input required id='sandbox-vendor-auth-code' name='sandbox.vendorAuthCode' value='${sandbox.vendorAuthCode}' type='password'></input>

        <label for='sandbox-plan-id'>Subscription Plan ID <small><a href='https://sandbox-vendors.paddle.com/subscriptions/plans' target='_blank'>(from here)</a></small></label>
        <input required id='sandbox-plan-id' name='sandbox.planId' value='${sandbox.planId}' type='text'></input>
      </section>

      <section id='live'>
        <h3>Live environment</h3>

        <if ${live.setupAttempted && !live.ok}>
          <p class='incorrect'>${live.error}</p>
        </if>

        <if ${live.ok}>
          <p class='correct'>Live environment settings are correct.</p>
        </if>

        <label for='live-vendor-id'>Vendor ID <small><a href='https://vendors.paddle.com/authentication' target='_blank'>(from here)</a></small></label>
        <input required id='live-vendor-id' name='live.vendorId' value='${live.vendorId}' type='text'></input>

        <label for='live-vendor-auth-code'>Vendor Auth Code <small>(secret API Key; <a href='https://vendors.paddle.com/authentication' target='_blank'>from here</a>)</small></label>
        <input required id='live-vendor-auth-code' name='live.vendorAuthCode' value='${live.vendorAuthCode}' type='password'></input>

        <label for='live-plan-id'>Subscription Plan ID <small><a href='https://vendors.paddle.com/subscriptions/plans' target='_blank'>(from here)</a></small></label>
        <input required id='live-plan-id' name='live.planId' value='${live.planId}' type='text'></input>
      </section>
      <button type='submit'>Save</button>
    </form>
  `
}
