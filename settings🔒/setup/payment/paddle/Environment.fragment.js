// @ts-check

import { html } from '@small-web/kitten'
import { db } from '@app/database'
import Switch from '@app/switch'

export default function () {
  const environment = db.settings.payment.providers.paddle.environment.current

  return html`
    <section id='environment'>

      <h3>
        <if ${environment === 'live'}>
          You’re live!
        <else>
          You’re using the sandbox.
        </if>
      </h3>

      <form method='POST' action='/settings/setup/payment/paddle/environment/'>
        <${Switch}
          name='live'
          label='Live (start taking actual payments)'
          checked=${environment === 'live'}

          hx-post='/settings/setup/payment/paddle/environment/'
          hx-trigger='change delay:330ms'
          hx-target='closest section'
          hx-swap='outerHTML'
          hx-include='#environment'
        />
        <button type='submit' x-data x-show='false'>Save</button>
      </form>

      <if ${environment === 'live'}>
        <then>
          <p>Your Small Web hosting service is <strong>live</strong>. You will be receiving payments in your Paddle account as people order servers.</p>
        </then>
        <else>
          <p>In the sandbox environment no actual payments will be taken. Please make sure everything works as expected before going live.</p>
        </else>
      </if>

    </section>
  `
}
