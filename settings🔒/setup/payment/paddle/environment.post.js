// @ts-check

import { db } from '@app/database'
import Mode from './Environment.fragment.js'

export default function ({ request, response }) {
  const isLive = request.body.live === 'on'
  db.settings.payment.providers.paddle.environment.current = isLive ? 'live' : 'sandbox'
  response.end(Mode())
}
