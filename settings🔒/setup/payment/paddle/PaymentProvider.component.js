// @ts-check

import { html } from '@small-web/kitten'
import { db } from '@app/database'

import Environment from './Environment.fragment.js'
import Form from './Form.fragment.js'

export default ({session}) => {
  return html`
    <div class='PaymentProviderPaddle'>
      <if ${!db.settings.dns.ok}>
        <then>
          <h3>Action required</h3>
          <p>You must <a href='/settings/setup/dns'>configure your DNS settings</a> before you can configure the Paddle payment provider.<p>
          <p>(The process includes webhooks and your server must be accessible by its domain.)</p>
        </then>
        <else>
          <markdown>

            ### Paddle

            [Paddle](https://paddle.com) is a [merchant of record](https://www.paddle.com/blog/what-is-merchant-of-record). (They sell the subscriptions for you and handle everything, including global tax remittance.)
    
            [Paddle pays you on the 1st and 15th of the month](https://www.paddle.com/help/manage/get-paid/when-and-how-do-i-get-paid). You must pay corporation tax on this income.

            **If you are making money with your Small Web Domain, please consider sharing a percentage of your earnings with [Small Technology Foundation](https://small-tech.org/) by [becoming a patron](https://small-tech.org/fund-us) so we can continue to develop the software you use to host your community.**
          </markdown>

          <if ${db.settings.payment.providers.paddle.ok}>
            <then>
              <p class='correct'>Your Paddle settings are correct.</p>
              <${Environment} />
            </then>
          </if>

          <markdown>
            #### Instructions

            1. Get a [Paddle](https://paddle.com) account.

            2. [Read Stay Small!](#stay-small) and Paddle’s [Data Processing Addendum (GDPR)](https://www.paddle.com/legal/data-processing-addendum).

            3. __Generate your API keys__ in both the [sandbox](https://sandbox-vendors.paddle.com/authentication) and [live](https://vendors.paddle.com/authentication) environments.

            4. __Create a subscription plan__ in both the [sandbox](https://sandbox-vendors.paddle.com/subscriptions/plans) and [live](https://vendors.paddle.com/subscriptions/plans) environments.

                The _plan name_, _custom message_, and _icon_ are not important as they’re overriden by Domain. Set them to values that make sense for you.

                Set the other options according to the table below:

                | Setting                     | Value       |
                | --------------------------- | ----------- | 
                | Billing Interval            | Monthly     |
                | Trial Period                | 0           |
                | Enable quantity (checkbox)  | _unchecked_ |
    
                Finally, under _Subscription Plan Pricing_, set your default currency and price (please make this a round number; [don’t use psychological pricing to manipulate people](#stay-small)) and check any other currencies you want to support (Paddle handles currency conversions for you automatically at checkout).

            5. __Enter your API and subscription details__ from your Paddle account __into the form below.__
          </markdown>

          <${Form} session=${session} />

          <section>
            <markdown>
              ### Stay small!

              Commercial payments support in Domain is designed for small organisations that serve their communities.

              It is simple by design. You only have one plan and we ask that you please also set prices in whole numbers (don’t use so-called “psychological pricing” to manipulate people).
    
              **These limitations are not bugs, they are features to encourage a Small Web.**

              The idea is that no single Small Web Domain should scale beyond a certain point. Your Small Web Domain should be serving your community and you should let other Small Web Domains serve theirs. This is our [non-colonial approach](https://small-web.org/about/#small-technology) as per the [Small Technology Principles](https://small-web.org/about/#small-technology).

              Support for a commercial option is necessary for organisations that have to exist under capitalism. It doesn’t mean we have to play their shortsighted manipulative games or adopt their success criteria. The goal is for our organisations to provide a bridge to a post-capitalist future (e.g., one where cities can use tokens to provide their citizens with access to the commons from the commons, etc.).

              You will not become rich by running a Small Web Domain. If that’s your goal, please look elsewhere. However, you will hopefully be able to subsist under capitalism while helping bootstrap a kinder, fairer, and more caring world based on respect for human rights and democracy.
            </markdown>
          </section>
        </else>
      </if>
    </div>

    <style>
      .PaymentProviderPaddle .paddleMode {
        display: block;
        border: 1px solid gray;
        border-radius: 1em;
        box-shadow: 0.25em 0.25em 0.25em #ccc;
        margin-bottom: 1em;
        padding-top: 1em;
        margin-left: -1em; padding-left: 1em;
        margin-right: -1em; padding-right: 1em;
      }

      .PaymentProviderPaddle details h3 {
        display: inline;
      }

      .PaymentProviderPaddle .paddleMode h4 {
        margin-top: 0;
        font-size: 1.5em;
      }

      .PaymentProviderPaddle summary {
        font-size: 2em;
        font-weight: bold;
        margin-bottom: 1em;
      }

      .PaymentProviderPaddle .paddleMode:last-of-type {
        margin-bottom: 1.5em;
      }

      .PaymentProviderPaddle table { border: 1px solid black; width: 100%; border-collapse: collapse; }
      .PaymentProviderPaddle th, .PaymentProviderPaddle td { padding: 0.25em 1em; }
      .PaymentProviderPaddle th { background: black; color: white; } 
      .PaymentProviderPaddle td { margin: 0; }
      .PaymentProviderPaddle td:first-of-type { white-space: nowrap; }
      .PaymentProviderPaddle td:nth-of-type(2) { width: 99%; }
      .PaymentProviderPaddle tr:nth-of-type(2n) { background: #eee; }
    </style>
  `
}
