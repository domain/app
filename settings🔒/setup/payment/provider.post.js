// @ts-check
import { db, PaymentProvider } from '@app/database'
import Provider from './Provider.fragment.js'
import { redirect, getRequiredParametersFromRequest } from '../../lib/utilities.script.js'

export default function ({ request, response }) {
  const { paymentProvider } = /** @type {{paymentProvider: string}} */
    (getRequiredParametersFromRequest(request, ['paymentProvider']))

  db.settings.payment.selectedProvider = paymentProvider
  db.settings.payment.ok = /** @type {PaymentProvider} */ (db.settings.payment.providers[db.settings.payment.selectedProvider]).ok

  if (request.headers['hx-request']) {
    // Since parts of the navigation are dependent on the setup state
    // (the “Places” section is disabled when setup is not valid, etc.),
    // we don’t just return the Provider fragment but ask for the whole
    // page to be swapped in so the navigation has a chance to update
    // its appearance and functionality.
    //
    // (Using HX-Location allows us to avoid a page refresh.)
    response.setHeader('HX-Location', '/settings/setup/payment/')
  } else {
    redirect('/settings/setup/payment', response)
  }
}
