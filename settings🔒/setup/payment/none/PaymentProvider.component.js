// @ts-check
import { markdown } from '@small-web/kitten'

export default _ => markdown`
    ### Private instance
    
    <p class='correct'>Your payment settings are correct.</p>

    You do not need to set up a payment method to use Domain.

    When no payment method is set, you can [create places](/settings/places/create) from within the Settings section.

    (This is a good option if you want to set up servers for yourself, your family and friends, or for your organisation.)
  `
