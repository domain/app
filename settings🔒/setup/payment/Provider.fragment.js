// @ts-check

import { html } from "@small-web/kitten"
import { db } from '@app/database'

import PaymentProviderNone from './none/PaymentProvider.component.js'
import PaymentProviderTokens from './tokens/PaymentProvider.component.js'
import PaymentProviderPaddleComponent from './paddle/PaymentProvider.component.js'

const PaymentProviderComponents = {
  none: PaymentProviderNone,
  tokens: PaymentProviderTokens,
  paddle: PaymentProviderPaddleComponent
}

export default function ({session}) {
  return html`
    <page htmx>
    <div class='PaymentProvider'>
      <form method='POST' action='/settings/setup/payment/provider/'>
        <label for='payment-provider'>Provider</label>
        <select
          name='paymentProvider'
          id='payment-provider'

          hx-post='/settings/setup/payment/provider'
          hx-trigger='change'
          hx-target='closest .PaymentProvider'
        >
          ${db.settings.payment.providerNames.map(provider => html`
            <option
             value=${provider}
             ${provider === db.settings.payment.selectedProvider && 'selected'
            }>
              ${/** @type {import('@app/database').PaymentProvider} */ (db.settings.payment.providers[provider]).name}
            </option>
          `)}
        </select>
        <button type='submit' x-data x-show='false'>Save</button>
      </form>
      ${[html`<${PaymentProviderComponents[db.settings.payment.selectedProvider]} session=${session}/>`]}
    </div>
  `
}
