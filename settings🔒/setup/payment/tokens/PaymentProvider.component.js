// @ts-check
import { markdown } from '@small-web/kitten'

export default _ => markdown`
    ### Tokens

    <p class='incorrect'>Not implemented yet.</p>

    **Not implemented yet:** Tokens are an alternative to using regular currency, credit/debit card transactions to provide access to servers. A munipicality, for example, might decide that it is a human right for every one of its citizens to have their own place on the Small Web. In this case, a munipicality might decide to issue tokens to every resident that they can use when setting up their place. The same municipality may also activate Stripe payments for those who want more than one site, etc.
  `
