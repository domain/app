// @ts-check

// Redirect calls to /admin to /settings/setup/organisation (first entry in nav).
export default function ({ request, response }) {
  response.writeHead(303, { Location: '/settings/setup/organisation' })
  response.end()
}
