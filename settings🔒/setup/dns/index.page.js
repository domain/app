// @ts-check
import { HTMX, html } from '@small-web/kitten'
import { db } from '@app/database'

import Settings from '../../Settings.layout.js'
import ServiceState from '../ServiceState.script.js'

export default function () {
  const state = new ServiceState()

  if (!db.settings.dns.setupAttempted) {
    state.set(state.states.UNKNOWN)
  } else if (db.settings.dns.ok) {
    state.set(state.states.OK)
  } else {
    state.set(state.states.NOT_OK, {
      error: db.settings.dns.error
    })
  }

  return html`
    <${Settings} activeSection='Domain Name Service (DNS)'>
      <h3>DNSimple</h3>

      <section class='instructions'>
        <h4>Instructions</h5>
        <ol>
          <li>Get a <a href='https://dnsimple.com' target='_blank'>DNSimple</a> account (a personal account should suffice as you only need to add subdomains to one domain).</li>
          <li><strong>DNSimple does not provide GDPR Data Protection Agreements for anything less than their $300/mo business accounts.</strong> They say one is not necessary for hosting subdomains. (see <a href='https://blog.dnsimple.com/2018/05/gdpr/' target='_blank'>GDPR at DNSimple</a>, <a href='https://dnsimple.com/privacy' target='_blank'>DNSimple Privacy Policy</a>).</li>
          <li>Add your domain to your DNSimple dashboard and find the details required on it under <strong>Account → Access Tokens</strong>.</li>
        </ol>
      </section>

      <if ${!state.is(state.states.UNKNOWN)}>
        <if ${state.is(state.states.OK)}>
          <p class='correct'>Your DNS settings are correct.</p>
          <else>
            <if ${state.states.NOT_OK.error !== null}>
              <p class='incorrect'>${state.states.NOT_OK.error}</p>
            </if>
        </if>
      </if>

      <form method='POST' action='/settings/setup/dns'>
        <label for='domain'>Domain</label>
        <input id='domain' name='domain' type='text' value='${db.settings.dns.domain}' required>

        <label for='accountId'>Account ID</label>
        <input id='accountId' name='accountId' type='password'  value='${db.settings.dns.accountId !== 0 ? db.settings.dns.accountId : ""}' required>

        <label for='accessToken' class='block'>Access Token</label>
        <input id='accessToken' name='accessToken' type='password' value='${db.settings.dns.accessToken}' required>

        <button type='submit'>Save</button>
      </form>
    </>
  `
}
