// @ts-check
import { db } from '@app/database'

import { unauthorised, redirectToPage, getRequiredParametersFromRequest } from '../../lib/utilities.script.js'

export default async function ({ request, response }) {
  // Validate and reference required parameters.
  const { domain, accountId, accessToken } = getRequiredParametersFromRequest(request, ['domain', 'accountId', 'accessToken'])
  if (domain === undefined) { return unauthorised(response) }

  // Validate passed data using DNSimple API.
  const retrieveDomainUrl = `https://api.dnsimple.com/v2/${accountId}/domains/${domain}`
  
  db.settings.dns.setupAttempted = true
  
  let dnsAccountDetails = {}
  try {
    dnsAccountDetails = await (await fetch(retrieveDomainUrl, {
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${accessToken}`
      }
    })).json()
  } catch (error) {
    dnsAccountDetails.message = `Server error: could not connect to DNS API. Please try again later.`
    console.error(error.message, error.cause)
  }

  // Save the state and error message (if any).
  if (dnsAccountDetails.message) {
    // Something went wrong (most likely an authentication failure)
    db.settings.dns.ok = false
    db.settings.dns.error = dnsAccountDetails.message
  } else {
    db.settings.dns.ok = true

    // Not setting to null due to https://codeberg.org/small-tech/jsdb/issues/2
    // TODO: Update once that’s fixed.
    db.settings.dns.error = ''
  }

  // Save the rest of the data.
  db.settings.dns.domain = domain
  db.settings.dns.accountId = Number(accountId)
  db.settings.dns.accessToken = accessToken

  redirectToPage({ request, response })
}
