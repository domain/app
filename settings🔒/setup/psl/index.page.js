// @ts-check
import { db } from '@app/database'
import { html } from '@small-web/kitten'

import Settings from '../../Settings.layout.js'
import ServiceState from '../ServiceState.script.js'

export default async function () {
  const state = new ServiceState()

  if (db.settings.payment.selectedProvider === 'none' /* none */)
  {
    // The Public Suffix List requirement for Small Web Domains is only for
    // public instances, not private ones. Private instances are ones where there is no
    // payment type set and where all registrations must be done via the administration
    // interface. For more information on the rationale, please see the PSL tab in the
    // administration interface.
    state.set(state.states.OK, { isPrivateInstance: true })
  }
  else if (db.settings.psl.ok)
  {
    // Once a domain is on the PSL, there is very little chance that it will be
    // removed so we can use the cached value here and not download the actual
    // list every time. If necessary, in the future we can add a retry option that
    // forces us to reconnect and check.
    state.set(state.states.OK, { isPrivateInstance: false })
  }
  else
  {
    // PSL is not valid, we need to validate it by downloading the PSL
    // and searching for the domain in it.
    let response
    try {
      response = await fetch('https://publicsuffix.org/list/public_suffix_list.dat')
    } catch (error) {
      response = null
      console.error(error.message, error.cause)
    }

    if (response == null) {
      state.set(
        state.states.NOT_OK,
        { error: `Server error: could not download Public Suffix List. Please try again later.`}
      )
    } else if (response.status !== 200) {
      // There was an error downloading the list. Cannot continue.
      state.set(
        state.states.NOT_OK,
        { error: `Could not download Public Suffix List. (${response.status}: ${response.statusText})`}
      )
    } else {
      const publicSuffixList = await response.text()
      const domainIsOnPublicSuffixList = publicSuffixList.split('\n').includes(db.settings.dns.domain)

      if (domainIsOnPublicSuffixList) {
        db.settings.psl.ok = true
        state.set(state.states.OK, { isPrivateInstance: false })
      } else {
        db.settings.psl.ok = false
        state.set(
          state.states.NOT_OK,
          { error: `Domain (${db.settings.dns.domain}) is not on the Public Suffix List (https://publicsuffix.org/list/public_suffix_list.dat).` }
        )
      }
    }
  }

  return html`
    <${Settings} activeSection='Public Suffix List (PSL)'>
      <if ${state.is(state.states.OK)}>
        <then>
          <if ${state.states.OK.isPrivateInstance}>
            <then>
              <p class='correct'>Private instances do not have be registered on the <a href='https://publicsuffix.org'>Public Suffix List</a>.</p>
              <p>A private instance is one where the payment provider is set to “none” and where domains can only be registered using this administration panel.</p>
              <p>The assumption with private instances is that all subdomains registered on the domain belong to the organisation running the Small Web Domain. If this assumption is incorrect for your use case (and domains are owned by separate entities even though you register them manually from here), you should <a href='https://github.com/publicsuffix/list/wiki/Guidelines'>apply to have your domain added to the PSL.</a></p>
              <p>If you’re not on the PSL, realise that this will allow the domain to set privacy-violating “supercookies” that are valid for all subdomains.</p>
              <p><a href='https://publicsuffix.org/learn/'>Learn more.</a></p>
            <else>
              <p class='correct'><strong>Your domain is on the Public Suffix List.</strong></p>
          </if>
        </then>
        <else>
          <p class='incorrect'><strong>${state.states.NOT_OK.error}</strong></p>

          <form method='GET' action='/settings/setup/psl'>
            <button>Check again now</button>
          </form>
    
          <section class='information'>
            <h3>Information</h3>
            <p>Public instances must be registered on the <a href='https://publicsuffix.org'>Public Suffix List</a> for privacy purposes.</p>
            <p>A public instance is one where the payment provider is set to anything but “none” where members of the public can register their own indepenedent Small Web places using tokens, money, etc.</p>
            <p>Without this requirement, the organisation running the Small Web Domain could set a “supercookie” on the main domain and violate the privacy of the people who own and control the subdomains.</p>
            <p>Additionally, having the domain on the PSL enables browsers to highlight the most important part of a domain name when displaying it and accurately sort history entries by site.</p>
            <p><a href='https://publicsuffix.org/learn/'>Learn more.</a></p>
          </section>

          <section class='instructions'>
            <h3>Instructions</h3>
            <ol>
              <li><a href='https://github.com/publicsuffix/list/wiki/Guidelines'>Read the guidelines</a> for submitting a domain to the <a href='https://publicsuffix.org'>Public Suffix List</a>.</li>
              <li>
                <p><a href='https://github.com/publicsuffix/list/pulls'>Submit your pull request</a> to amend the PSL.</p>
                <p>You can use <a href='https://small-web.org'>small-web.org’s pull request</a> as a template. You can also refer to that pull request in yours as an example of a precedent for acceptance of a Small Web Domain onto the Public Suffix List.</p>
                <p>If you have any touble getting accepted, please contact <a href='https://small-tech.org'>Small Technology Foundation</a> at <a href='mailto:'>hello@small-tech.org</a> and we will help.</p>
              </li>
            </ol>
            <p><strong>Once your domain is on the public suffix list, we will automatically detect the fact and enable your Small Web Domain for public access.</strong></p>
          </section>
        </else>
      </if>
    </>
  `
}
