// @ts-check

import State from '@small-tech/state'

export default class ServiceState extends State {
  constructor () {
    super()
    this.states = {
      UNKNOWN: {},
      PROCESSING: {},
      OK: {},
      NOT_OK: {}
    }
    this.state = this.states.UNKNOWN
  }
}
