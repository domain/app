// @ts-check
import { html } from '@small-web/kitten'

import Application from '../Application.layout.js'
import Navigation from './Navigation.component.js'

// @ts-ignore CSS module.
import Styles from './Settings.fragment.css'

export default ({ SLOT, activeSection = '', autoHeading = true, ...props }) => html`
  <${Application} layout='settings' ...${props}>
    <${Navigation} activeSection=${activeSection} />
    <main id='content'>
      <if ${autoHeading}>
        <h2>${activeSection}</h2>
      </if>
      ${SLOT}
    </main>
  </>

  <${Styles} />
`
