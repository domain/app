// @ts-check

// TODO: Review which of these utility functions actually belong
// on the request/response objects themselves (and should automatically
// be injected into them by Kitten.)

export function unauthorised (response) {
  response.writeHead(403)
}
  
export function redirect (path, response) {
  response.statusCode = 303
  response.setHeader('Location', path)
}

export function redirectToPage({ request, response }) {
  redirect(request.path, response)
}

export function getRequiredParametersFromRequest(request, parameterList) {
  const ERROR_INDICATOR = {}
  if (!request.body) return ERROR_INDICATOR
  const parameterObject = {}
  parameterList.forEach(parameterName => {
    const parameterValue = request.body[parameterName]
    if (parameterValue === undefined) {
      return ERROR_INDICATOR
    }

    // Support one level of object nesting in forms.
    // e.g., sandbox.planId will result in { sandbox: { planId: '…' }}
    const keys = parameterName.split('.')
    if (keys.length === 2) {
      if (parameterObject[keys[0]] === undefined) {
        parameterObject[keys[0]] = {}
      } 
      parameterObject[keys[0]][keys[1]] = parameterValue
    } else {
      parameterObject[parameterName] = parameterValue
    }
  })
  return parameterObject
}

export function getOptionalParametersFromRequest(request, parameterList) {
  const NO_PARAMETERS = {}
  if (!request.body) return NO_PARAMETERS
  const parameterObject = {}
  parameterList.forEach(parameterName => {
    const parameterValue = request.body[parameterName]

    // Support one level of object nesting in forms.
    // e.g., sandbox.planId will result in { sandbox: { planId: '…' }}
    const keys = parameterName.split('.')
    if (keys.length === 2) {
      if (parameterObject[keys[0]] === undefined) {
        parameterObject[keys[0]] = {}
      } 
      parameterObject[keys[0]][keys[1]] = parameterValue
    } else {
      parameterObject[parameterName] = parameterValue
    }
  })
  return parameterObject
}

export function getParameterObjectFromRequest ({ request, requiredParameters = [], optionalParameters = [] }) {
  let _requiredParameters, _optionalParameters
  if (requiredParameters.length > 0) {
    _requiredParameters = getRequiredParametersFromRequest(request, requiredParameters)
    // Our indicator that there’s been an error is an empty object returned from the call.
    if (Object.keys(_requiredParameters).length === 0) {
      throw new Error('Required parameters not found.')
    }
  }
  if (optionalParameters.length > 0) {
    _optionalParameters = getOptionalParametersFromRequest(request, optionalParameters)
  }
  const parameterObject = Object.assign({}, _requiredParameters, _optionalParameters)
  return parameterObject
}
