// @ts-check

import db from "@app/database"
import { DNSimple } from "dnsimple"
import HetznerCloud from 'hcloud-js'

/**
  Deletes the provided subdomain.

  @param {string} subdomain
  @throws if domain cannot be found.
*/
export default async function deleteDomain (subdomain) {
  const domainToDelete = db.domains[subdomain]
  if (domainToDelete  == undefined) {
    throw new Error(`Could not find domain to delete (${subdomain})`)
  }

  // Delete DNS entry.
  const dnsHost = new DNSimple({
    accessToken: db.settings.dns.accessToken
  })
  if (domainToDelete.dns !== undefined) {
    try {
      await dnsHost.zones.deleteZoneRecord(
        db.settings.dns.accountId,
        db.settings.dns.domain,
        domainToDelete.dns.id
      )
      console.info(`DNS deleted: ${subdomain}`)
    } catch (error) {
      console.warn(`Was not able to delete DNS for domain ${subdomain}. Ignoring error.`, error)
    }
  } else {
    console.warn(`No DNS entry to delete for domain ${subdomain}. Ignoring error.`)
  }

  // Remove VPS.
  const serverHost = new HetznerCloud.Client(db.settings.vps.apiToken)
  let serverDeleteResult
  try {
    serverDeleteResult = await serverHost.servers.delete(domainToDelete.server.id)
    console.info(`Server deleted: ${subdomain}`, serverDeleteResult)
  } catch (error) {
    console.warn(`Was not able to delete VPS for domain ${domainToDelete}. Ignoring error.`, error)
  }

  // Actually delete the record from the database.
  delete db.domains[subdomain]
  console.info(`Domain entry deleted from database: ${subdomain}`)
}
