// @ts-check
import { html } from '@small-web/kitten'

import Places from './Places.layout.js'

export default ({ SLOT, returnUrl, ...props }) => html`
  <${Places} ...${props}>
    <p><a href='${returnUrl}'>Return to the list.</a></p>
    ${SLOT}
  </>
`
