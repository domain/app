// @ts-check

// TODO: Copied from Applications.layout; refactor.
import { html } from '@small-web/kitten'

// Applications layout extends Settings layout.
import Settings from '../Settings.layout.js'

export default ({ SLOT, ...props }) => html`
  <${Settings} activeSection=View autoHeading=false ...${props}>
    ${SLOT}
  </> 
`
