export default ({ request, response }) => {
  const domain = request.params.domain
  if (domain === undefined) {
    return response.notFound()
  }

  const domainRecord = db.domains[domain]
  domainRecord.setup.complete = true
  domainRecord.setup.state = 'place-is-reachable'
  domainRecord.setup.endedAt = new Date()

  response.get('/settings/places/troubleshoot/')
}
