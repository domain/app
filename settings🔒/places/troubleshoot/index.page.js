// @ts-check
import { HTMX, html } from '@small-web/kitten'
import { db } from '@app/database'

import Places from '../Places.layout.js'
import ok from '../../setup/ok.script.js'

export default function () {
  const setupOk = ok()

  // Get list of registered domains.
  // TODO: Create an index for completed and non-completed domain setups
  // so we don’t have to do this every time.
  // TODO: Page this.
  // @ts-ignore proxy mixin methods
  const domains = Object.values(db.domains).filter(domain => !domain.setup.complete)

  const prettyStateName = stateName => {
    const stateNameWithoutDashes = stateName.replace(/-/g, ' ')
    return stateNameWithoutDashes[0].toUpperCase() + stateNameWithoutDashes.slice(1) + '.'
  }

  return html`
    <${Places} activeSection=Troubleshoot>
      <h2>Troubleshoot places</h2>

      <if ${setupOk}>
        <then>
          <p>The setup of the following places is marked as incomplete.</p>
          <p>This may be due to an error or a communication issue between the place and this Domain instance.</p>
          <if ${domains.length > 0}>
            <then>
              ${
                domains.map((domain, index) => html`
                  <section class='domain'>
                    <h3>Domain: ${domain.subdomain}</h3>
                    <table>
                      <tbody>
                        <tr>
                          <td>Application:</td>
                          <td><a href='${domain.application.source}' target='_blank'>${domain.application.name}</a></td>
                        </tr>
                        <tr>
                          <td>Error:</td>
                          <td>${domain.setup.hasError ? domain.setup.error : 'No' }</td>
                        </tr>
                        <tr>
                          <td>Status:</td>
                          <td>${prettyStateName(domain.setup.state)}</td>
                        </tr>
                      </tbody>
                    </table>
                    <h4>Actions</h4>
                    <ul>
                      <li>
                        <a
                          class='action'
                          href='https://${domain.subdomain}.${db.settings.dns.domain}'
                          target='_blank'
                        >View</a>
                      </li>
                      <li>
                        <a
                          class='action'
                          href='/settings/places/troubleshoot/mark-as-reachable/${domain.subdomain}'
                        >Mark as reachable</a>
                      </li>
                      <li>
                        <a
                          class='action'
                          href='/settings/places/delete/${domain.subdomain}/troubleshoot'
                        >Delete</a>
                      </li>
                    </ul>
                  </section>
                `)
              }
            </then>
            <else>
              <p><strong>Nothing yet.</strong></p>
            </else>
        </then>
        <else>
          <p>Your instance is not set up properly.</p>
          <p><strong>Please <a href='/settings/setup'>finish setting up</a> </strong></p>
        </else>
      </if>
    </>
    <style>
      section.domain {
        border-top: 1px dashed grey;
        padding-top: 1em;
        padding-bottom: 2em;
      }

      section.domain:last-of-type {
        border-bottom: 1px dashed grey;
      }

      section.domain h3 {
        margin-top: 0;
      }

      section.domain h4 {
        margin-top: 0.5em;
        margin-bottom: 0.25em;
      }

      section.domain ul {
        list-style-type: none;
        margin-left: -2.5em;
        margin-bottom: 0;
      }

      section.domain li {
        display: inline-block;
        margin-left: 0;
        margin-right: 1em;
        margin-bottom: 0;
      }

      section.domain td:first-of-type {
        font-weight: bold;
      }

      section.domain td:last-of-type {
        width: auto;
      }
    </style>
  `
}
