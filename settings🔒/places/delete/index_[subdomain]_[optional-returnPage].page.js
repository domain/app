// @ts-check
import { db } from '@app/database'
import { HTMX, html } from '@small-web/kitten'

import PlacesAction from '../PlacesAction.layout.js'
import { redirect } from '../../lib/utilities.script.js'

const placesPage = '/settings/places/'
const troubleshootPage = '/settings/places/troubleshoot'

export default function ({ request, response }) {
  let error
  if (request.session.error !== undefined) {
    error = request.session.error
    delete request.session.error
  }

  const subdomain = request.params.subdomain
  
  if (subdomain == undefined) {
    redirect(placesPage, response)
    return
  }

  let returnUrl = ''
  const returnPage = request.params.returnPage
  switch (returnPage) {
    case 'troubleshoot':
      returnUrl = troubleshootPage
      break
    default:
      returnUrl = placesPage
  }

  return html`
    <${PlacesAction} returnUrl=${returnUrl}>
      <form method='POST' action='/settings/places/delete/${subdomain}/${returnPage}'>
        <h3>Delete place</h3>
        <p>Are you sure you want to delete <a
          href='https://${subdomain}.${db.settings.dns.domain}'
          target='_blank'
        >${subdomain}?</a></p>
        <if ${error !== undefined}>
          <dialog open>
            <p>${error}</p>
            <button>OK</button>
          </dialog>
        </if>
        <div id='actions'>
          <button id='cancel' name='action' value='cancel' type='submit' default>No</button>
          <button id='confirm' name='action' value='confirm' type='submit'>Yes</button>
        </div>
      </form>
    </>

    <style>
      #actions {
        display: flex;
        column-gap: 1em;
      }
    </style>
  `
}
