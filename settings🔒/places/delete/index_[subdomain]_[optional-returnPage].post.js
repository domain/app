// @ts-check
import HetznerCloud from 'hcloud-js'
import { DNSimple } from 'dnsimple'
import { db } from '@app/database'
import { redirect, redirectToPage } from '../../lib/utilities.script.js'
import deleteDomain from '../../lib/deleteDomain.script.js'
import { html } from '@small-web/kitten'

const placesPage = '/settings/places/'
const troubleshootPage = '/settings/places/troubleshoot'

const serverHost = new HetznerCloud.Client(db.settings.vps.apiToken)
const dnsHost = new DNSimple({
  accessToken: db.settings.dns.accessToken
})

export default async function ({ request, response }) {
  let returnUrl = ''
  console.log(request.params)
  const returnPage = request.params.returnPage
  switch (returnPage) {
    case 'troubleshoot':
      returnUrl = troubleshootPage
      break
    default:
      returnUrl = placesPage
  }

  const subdomain = request.params.subdomain
  if (subdomain == undefined) {
    redirect(returnUrl, response)
    return
  }
  
  if (!request.body || !request.body.action) {
    // Invalid request.
    redirect(returnUrl, response)
    return
  }
  
  if (request.body.action === 'confirm') {
    // Actually delete the item.
    try {
      await deleteDomain(subdomain)
    } catch (error) {
      console.error(error)
    }
  }
  
  redirect(returnUrl, response)
}
