// @ts-check
import { db, Application } from '@app/database'
import { kitten, html, events, _db } from '@small-web/kitten'

import Registrar from '../../../RegistrationService.script.js' 
import Settings from '../../Settings.layout.js'
import RegistrationForm, { AppSelector, RegistrationButton } from './RegistrationForm.component.js'
import { RegistrationFormState } from './RegistrationForm.component.js'

// @ts-ignore CSS fragment.
import Styles from './index.fragment.css'

import ok from '../../setup/ok.script.js'
import ProgressIndicator from './ProgressIndicator.component.js'
import HetznerCloud from 'hcloud-js'
import { DNSimple } from 'dnsimple'

export default async function ({ request }) {
  const setupOk = ok()

  // Get a reference to the session object in our own database. 
  // (And create the sessions object if it doesn’t already exist.)
  const sessionId = request.session.id
  if (db.sessions[sessionId] === undefined) {
    db.sessions[sessionId] = {}
  }
  const session = db.sessions[sessionId]
  
  // Start with a fresh state on page load.
  const state = new RegistrationFormState()
  state.set(state.states.INITIAL)
  session.registrationFormState = state

  return html`
    <${Settings} activeSection=Create autoHeading=false>
      <page>
      <h2>Create a place</h2>
      <if ${!setupOk}>
        <then>
          <p>Your instance is not set up properly.</p>
          <p><strong>Please <a href='/settings/setup'>finish setting up</a> </strong></p>
        </then>
        <else>
          <if ${process.env.HTMX_LOGGER}>
            <then>
              <!-- Only include HTMX logging when requested. -->
              <content for='AFTER_LIBRARIES'>
                <script>
                  htmx.logger = function(elt, event, data) {
                    if(console) {
                      console.log(event, elt, data)
                    }
                  }
                </script>
              </content>
            </then>
          </if>
          <${RegistrationForm} state=${session.registrationFormState} />
          <ul id='status'></ul>
          <${Styles} />
        </else>
      </if>
    </>
  `
}

const statusMessages = []
/** @param {string} message */
const status = message => {
  console.info(`🍭 ${message}`)
  statusMessages.push(message)
  return kitten.html`
    <ul id='status' morph>
      ${statusMessages.map(message => kitten.html`<li>${message}</li>`)}
    </ul>
  ` 
}


// // DEBUG: transition to registering state.
// export function onConnect() {
//   setTimeout(() => {
//     const state = new RegistrationFormState()
//     state.set(state.states.REGISTERING, { stage: 'Registering…' })
//     this.send(html`<${RegistrationForm} state=${state} />`)
  
//     setTimeout(() => {
//       state.set(state.states.REGISTERING, { stage: 'Server deployed.' })
//       this.send(html`<${RegistrationButton} state=${state} />`)

//       setTimeout(() => {
//         state.set(state.states.REGISTERING, { stage: 'Getting security certificate.' })
//         this.send(html`<${RegistrationButton} state=${state} />`)

//         setTimeout(() => {
//           const subdomain = 'dummy'
//           const domainToken = 'FEEDBEEF'
//         }, 5000)
//       }, 1000)
//     }, 3000)
//   }, 1000)
// }

/**
  Application source drop-down change.

  @param {{ applicationSource: string, subdomain: string }} data
  @this import('@small-web/kitten').KittenPage
*/
export async function onApplicationSource (data) {
  const session = db.sessions[this.request.session.id]

  const selectedApplicationIndex = db.settings.applications.list.findIndex(application => {
    return application.source === data.applicationSource
  })

  const selectedApplication = selectedApplicationIndex > -1
    ? db.settings.applications.list[selectedApplicationIndex]
    : new Application({ name: 'Custom', source: 'custom' }) 

  const state = session.registrationFormState || new RegistrationFormState()
  state.subdomain = data.subdomain
  state.application = selectedApplication

  this.send(kitten.html`<${AppSelector} state=${state} />`)
}

/**
  Custom application source drop-down change.

  @param {{ customSource: string }} data
  @this import('@small-web/kitten').KittenPage
*/
export function onCustomSource (data) {
  const session = db.sessions[this.request.session.id]
  const state = session.registrationFormState || new RegistrationFormState()
  state.application = new Application({ name: 'Custom', source: 'custom' })
  state.customApplicationSource = data.customSource
}

/**
  Subdomain search box change.

  @param {{ subdomain: string }} data
  @this import('@small-web/kitten').KittenPage
*/
export function onSubdomain (data) {
  const session = db.sessions[this.request.session.id]
  const state = session.registrationFormState || new RegistrationFormState()
  
  state.set(state.states.CHECKING_AVAILABILITY)

  this.page.send(html`<${ProgressIndicator} state='${state}' morph />`)
  
  // Via https://github.com/miguelmota/is-valid-hostname/blob/a375657352475b03fbd118e3b46029aca952d816/index.js#L5
  // implementation of RFC 3696. (With removal of dots for our purposes as we are validating a subdomain.)
  const validHostnameCharacters = /^([a-zA-Z0-9-]+){1,253}$/

  const { subdomain } = data
  state.subdomain = subdomain.toLowerCase()

  switch (true) {
    case (subdomain.trim() === ''):
      // Disallow queries comprised solely of whitespace.
      // (But no need to display an error, so we just set it back to the initial state.)
      state.set(state.states.INITIAL)
    break

    case (!validHostnameCharacters.test(subdomain)):
      // Disallow domains with invalid characters (RFC 3696).
      state.set(state.states.ERROR, {reason: 'Domain contains invalid characters.'})
    break

    case (db.domains[subdomain] !== undefined):
      // Domain is already registered.
      state.set(state.states.UNAVAILABLE)
    break

    default:
      // Domain is available.
      state.set(state.states.AVAILABLE)
    break
  }

  // Send the new form.
  this.page.send(RegistrationForm({state, partial: true}))
}

/**
  Register a domain and set up a place (Kitten instance and app) at it.

  @this import('@small-web/kitten').KittenPage
*/
export async function onRegister () {
  const session = db.sessions[this.request.session.id]
  const registrationFormState = session.registrationFormState

  if (registrationFormState === undefined) {
    // TODO: Handle this better.
    console.error('Registration form state is undefined, cannot continue.')
    return
  }

  const subdomain = registrationFormState.subdomain
  const applicationName = registrationFormState.application.name
  const gitHttpsCloneUrl = registrationFormState.customApplicationSource === '' ? registrationFormState.application.source : registrationFormState.customApplicationSource

  const registrar = Registrar.getInstance()

  this.addEventHandler(registrar, subdomain, event => {
    switch (event.type) {
      case Registrar.events.start:
        registrationFormState.set(registrationFormState.states.REGISTERING, { stage: 'Registering domain…' })
        this.send(html`<${RegistrationForm} state=${registrationFormState} />`)
        break

      case Registrar.events.progress:
        registrationFormState.set(registrationFormState.states.REGISTERING, { stage: event.message })
        this.send(html`<${RegistrationButton} state=${registrationFormState} />`)
        break

      case Registrar.events.error:
        this.send(status(event.message))
        break

      case Registrar.events.warning:
        console.warn(`Warning: ${event.warning}`)
        break

      case Registrar.events.success:
        registrationFormState.set(registrationFormState.states.INITIAL)
        this.send(html`<${RegistrationButton} state=${registrationFormState} />`)
        this.send(html`
          <div id='registration-details' morph>
            <h3>Your new place is ready!</h3>
            <ul>
              <li><a href='https://${subdomain}.${db.settings.dns.domain}' target='_blank'>View place</a></li>
              <li><a href='https://${subdomain}.${db.settings.dns.domain}/💕/hello/${event.domainToken}' target='_blank'>Create secret (single use)</a></li>
            </ul>
            <style>
              #registration-details {
                background-color: var(--background-alt);
                margin: 1em -1em;
                padding: 1em;
                border-radius: 0.5em;
              }
              #registration-details h3 {
                margin-top: 0;
              }
            </style>
          </div>
        `)
        break

      default:
        throw new Error(`Unexpected event type received from Registrar: ${event.type}`)
    }
  })

  // Start registering the domain.
  registrar.register({ subdomain, applicationName, gitHttpsCloneUrl })
}
