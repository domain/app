// @ts-check
import { html } from '@small-web/kitten'
import { db } from '@app/database'

import State from '@small-tech/state'
import ProgressIndicator from './ProgressIndicator.component.js'

// @ts-ignore CSS import.
import Styles from './RegistrationForm.fragment.css'

export class RegistrationFormState extends State {
  /** @type typeof import ('../../../app_modules/database/DNS.js').default.prototype.domain */
  $domain 
  /** @type import ('../../../app_modules/database/Application.js').default */
  $application
  subdomain = ''
  customApplicationSource = ''

  // The constructor is called by JSDB with a parameter object
  // when deserialising persisted objects.
  constructor (parameters = {}) {
    super ()
    this.states = {
      INITIAL: {},
      CHECKING_AVAILABILITY: {},
      AVAILABLE: {},
      UNAVAILABLE: {},
      ERROR: {},
      REGISTERING: {},
      REGISTERED: {}
    }

    Object.assign(this, parameters)

    // If we are called without parameters (i.e, not by
    // JSDB when deserialising one of our instances), set
    // the initial state.
    if (parameters.state === undefined) this.state = this.states.INITIAL
  }

  get fullDomain () {
    return `${this.subdomain}.${this.domain}`
  }

  // Since these properties access the database (db) itself, and since
  // the database doesn’t yet exist when instances of this model class
  // are being instantiated by JSDB when the database is being opened,
  // we set up the properties as accessors so we don’t get errors by
  // referring to db before it is initialised.

  get domain () {
    if (this.$domain === undefined) {
      this.$domain = db.settings.dns.domain
    }
    return this.$domain
  }

  set domain ($domain) {
    this.$domain = $domain
  }

  get application () {
    if (this.$application === undefined) {
      this.$application = db.settings.applications.list[0]
    }
    return this.$application
  }

  set application ($application) {
    this.$application = $application
  }
}

/**
  App Selector fragment.

  @param {{ state: RegistrationFormState }} props
*/
export function AppSelector ({ state }) {
  const applications = db.settings.applications.list.concat(
    [
      {
        name: 'Custom',
        source: 'custom'
      }
    ]
  )

  return html`
    <div id='appSelector' morph>
      <label for='app-source'>App</label>
      <select
        id='app-source'
        name='applicationSource'
        connect
        ${state.is(state.states.REGISTERING) ? 'disabled' : ''}
      >
       ${applications.map((application, _index) => html`
          <option
            value="${application.source}"
            ${state.application.source === application.source && 'selected'}
          >
            ${application.name}
          </option>
       `)}
      </select>
      <if ${state.application.source === 'custom'}>
        <then>
          <p>
            <label for='custom-source'>Source <span class='labelDetails'>(custom git repository link of app to install; starts with <code>https://</code> and ends with <code>.git</code>)</span></label>
            <input
              id='custom-source'
              type='url'
              name='customSource'
              hx-trigger='input changed delay:300ms'
              connect
              value='${state.customApplicationSource}'
            ></input>
          </p>
        </then>
        <else>
          <p id='source-detail'>
            Source: <a href='${state.application.source}' :href='source'>
            <span>${state.application.source}</span></a>
          </p>
        </else>
      </if>
    </div>
  `
}

/**
  Registration button that doubles as registration progress indicator.

  @param {{
    state: RegistrationFormState
  }} props
*/
export function RegistrationButton ({state}) {
  return html`
    <button
      id='register-button'
      name='register'
      type='submit'
      ${state.is(state.states.AVAILABLE) ? '' : 'disabled'}
      connect
      morph
    >
      ${state.is(state.states.REGISTERING) ? html`
        <${IndeterminateProgressIndicator} /> ${state.states.REGISTERING.stage}
      ` : 'Register' }
    <style>
      #register-button {
        transition: all 0.33s ease;
      }
    </style>
    <if ${state.is(state.states.REGISTERING)}>
      <style>
        #register-button[disabled] {
          opacity: 1 !important;
          background: var(--background-body);
          transform: translate(-3em);
          transition: all 0.33s ease;
        }
      </style>
    </if>
    </button>
  `
}

function IndeterminateProgressIndicator () {
  return html`
    <svg class='IndeterminateProgressIndicator' viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
      <title>Registration is in progress.</title>
      <g fill="none" stroke-width="1.5" stroke="currentColor">
        <path stroke-linecap="round" stroke-linejoin="round" d="M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path>
      </g>
      <g transform="translate(5 5) scale(0.6)">
        <g>
          <rect x="11" y="1" width="2" height="5" opacity=".14"/>
          <rect x="11" y="1" width="2" height="5" transform="rotate(30 12 12)" opacity=".29"/>
          <rect x="11" y="1" width="2" height="5" transform="rotate(60 12 12)" opacity=".43"/>
          <rect x="11" y="1" width="2" height="5" transform="rotate(90 12 12)" opacity=".57"/>
          <rect x="11" y="1" width="2" height="5" transform="rotate(120 12 12)" opacity=".71"/>
          <rect x="11" y="1" width="2" height="5" transform="rotate(150 12 12)" opacity=".86"/>
          <rect x="11" y="1" width="2" height="5" transform="rotate(180 12 12)"/>
          <animateTransform attributeName="transform" type="rotate" calcMode="discrete" dur="0.75s" values="0 12 12;30 12 12;60 12 12;90 12 12;120 12 12;150 12 12;180 12 12;210 12 12;240 12 12;270 12 12;300 12 12;330 12 12;360 12 12" repeatCount="indefinite"/>
        </g>
      </g>
      <style>
        .IndeterminateProgressIndicator {
          height: 1lh;
          vertical-align: middle;
        }
      </style>
    </svg>
  `
}

/**
  @function RegistrationForm
  @param {{
    state: RegistrationFormState,
    partial: boolean
  }} props
*/
export default ({ state, partial }) => {
  if (state === undefined) {
    state = new RegistrationFormState()
    console.log('creating new state', state)
  }

  // state.set(state.states.REGISTERING)

  return html`
    <div
      class='RegistrationForm'
      id='register-form'
      morph='config:{ignoreActive:true} swap:500ms'
    >
      <form name='registrationForm' id='registrationForm'>
        <p>A place is Small Web app made with Kitten installed on a subdomain on ${state.domain}.</p>

        <div id='create'>
          <${AppSelector} state=${state} />
          <label for='subdomain'>Subdomain</label>
          <input
            id='subdomain'
            name='subdomain'
            type='search'
            value=${state.subdomain}
            connect
            ${state.is(state.states.REGISTERING) ? 'disabled' : ''}
            trigger='keyup changed delay:500ms, search, DOMContentLoaded[document.getElementById("subdomain").value!=""] from:window'
          />

          <${ProgressIndicator} state=${state} partial=${partial} />
          <${RegistrationButton} state=${state} />
          <div id='registration-details' morph></div>
        </div>
      </form>

      <if ${!partial}>
        <${Styles} />
      </if>
    </div>
  `
}
