// @ts-check
import { html } from '@small-web/kitten'

/**
  A basic animated indeterminate progress indicator.

  @param {{
    state: import('./RegistrationForm.component.js').RegistrationFormState,
    id: string,
    partial: boolean
  }} parameters
*/
export default ({ state, id, partial=false }) => {
  let stateClass, content

  switch(state.state) {
    case state.states.INITIAL:
      stateClass = 'initial'
      content = 'Waiting for input'
    break

    case state.states.CHECKING_AVAILABILITY:
      stateClass = 'checking'
      content = 'Checking if domain is already taken…'
    break

    case state.states.ERROR:
      stateClass = 'error'
      content = `Error: ${state.states.ERROR.reason}`
    break

    case state.states.AVAILABLE || state.states.REGISTERING:
      stateClass = 'available'
      content = `${state.fullDomain} is available.`
    break

    case state.states.UNAVAILABLE:
      stateClass = 'unavailable'
      content = `${state.fullDomain} is not available.`
    break
  }

  return html`
    <div
      id='progress-indicator'
      class='ProgressIndicator
      ${stateClass}'
      morph
    >
      <progress id=${id}>Checking if domain is already taken…</progress>
      <p >${content}</p>
          <style>
            .ProgressIndicator p { margin-top: 0; }

            .ProgressIndicator.error p::before,
            .ProgressIndicator.available p::before,
            .ProgressIndicator.unavailable p::before {
              display: inline-block;
              margin-right: 0.25em;
            }

            .ProgressIndicator.initial p { opacity: 0; }
            .ProgressIndicator.error p { color: red; }
            .ProgressIndicator.error p::before {
              content: '❌';
            }
            .ProgressIndicator.available p { color: green; }
            .ProgressIndicator.available p::before {
              content: '✅';
            }
            .ProgressIndicator.unavailable { color: gray; }
            .ProgressIndicator.unavailable p::before {
              content: '😿';
            }

            .ProgressIndicator progress {
              position: relative;
              pointer-events: none; /* do not capture mouse events */
              width: 100%;
              z-index: 0;
              margin-top: 0;
              opacity: 0;
              transition: opacity 500ms ease-in;
            }

            .ProgressIndicator.checking progress {
              opacity: 1;
            }
          </style>
    </div>
  `
}
