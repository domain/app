// @ts-check

/**
  Returns a CloudInit configuration that sets up a Small Web server
  running the Kitten app at the provided git repository URL.

  Expects to be run on and currently only supported on AlmaLinux 9.

  (It should also work on Fedora and AlmaLinux 8.)

  @param {{
    token: string,
    subdomain: string,
    domain: string,
    callbackDomain: string,
    appGitHttpsUrl: string,
    sshKey: string
  }} parameters
 */

export default function cloudInit ({token, subdomain, domain, callbackDomain, appGitHttpsUrl, sshKey}) {
  const cloudConfig = `#cloud-config

    # Configures a Small Web server running a Kitten app.
    users:
      - name: kitten
        gecos: Kitten
        sudo: ALL=(ALL) NOPASSWD:ALL
        lock_passwd: true
        ssh_authorized_keys:
          - '${sshKey}'
        groups: wheel
        shell: /bin/bash

    # TODO: Uncomment this before going live.
    # disable_root: true

    runcmd:
      #
      # Update package manager and install Kitten’s system dependencies.
      #
      - wget -O/dev/null -q https://${callbackDomain}/domain/${token}/status/installing-system-depedencies
      - dnf update -y
      - dnf install git tar --assumeyes
      #
      # Configure firewall (install firewalld and add http and https services).
      #
      - wget -O/dev/null -q https://${callbackDomain}/domain/${token}/status/configuring-firewall
      - dnf install firewalld -y
      - systemctl enable firewalld
      - systemctl start firewalld
      - firewall-cmd --zone=public --add-service=http --permanent
      - firewall-cmd --zone=public --add-service=https --permanent
      - firewall-cmd --reload
      #
      # Configure ssh (turn off password authentication).
      #
      - wget -O/dev/null -q https://${callbackDomain}/domain/${token}/status/configuring-ssh
      - sed -i 's/#PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config
      - systemctl reload sshd
      #
      # Ensure kitten account’s home directory permissions are correct.
      #
      - wget -O/dev/null -q https://${callbackDomain}/domain/${token}/status/setting-permissions
      - chown -R kitten:kitten /home/kitten
      #
      # Set hostname.
      #
      - wget -O/dev/null -q https://${callbackDomain}/domain/${token}/status/setting-hostname
      - hostnamectl set-hostname ${subdomain}.${domain}
      #
      # Set services to linger for kitten account so the server survives log outs.
      #
      - wget -O/dev/null -q https://${callbackDomain}/domain/${token}/status/setting-services-to-linger
      - loginctl enable-linger kitten
      #
      # Improve the server’s ergonomics for when we ssh into it.
      #
      #   • Install and use fish shell as default.
      #   • Install fisher and useful fish plugins.
      #   • Add the epel-release dnf repository and install Helix Editor
      #     so we have a modern alternative to nano if we want to edit something.
      #   • Add a friendly welcome message for the shell with instructions
      #     on how to get started and where to get more information.
      #
      - wget -O/dev/null -q https://${callbackDomain}/domain/${token}/status/improving-server-experience
      # Install fish shell.
      - cd /etc/yum.repos.d/
      - wget https://download.opensuse.org/repositories/shells:fish:release:3/CentOS-9_Stream/shells:fish:release:3.repo
      - yum install fish -y
      # Add epel-release repository (necessary for Helix Editor).
      - dnf config-manager --set-enabled crb -y
      - dnf install epel-release -y
      # Add util-linux-user package (necessary for chsh used to set the default shell).
      - dnf install util-linux-user -y
      # Set the default shell to fish shell.
      - echo /usr/local/bin/fish | sudo tee -a /etc/shells
      - chsh --shell /usr/bin/fish kitten
      # Configure the fish greeting to show Kitten sixel and informative welcome message.
      - su kitten -c 'mkdir -p /home/kitten/.config/fish'
      - echo -e 'if status is-interactive\\n\\tfunction fish_greeting\\n\\t\\techo ""\\n\\t\\tcat /home/kitten/.local/share/small-tech.org/kitten/app/kitten-day.sixel\\n\\t\\techo -e "\\\\n\\\\e[1;31mWelcome to your 💕 Small Web place!\\\\e[0m"\\n\\t\\techo -e "\\\\n• Powered by Kitten \\\\e[3m(https://kitten.small-web.org)\\\\e[0m"\\n\\t\\techo -e "• Deployed by Domain \\\\e[3m(https://codeberg.org/domain/app)\\\\e[0m"\\n\\t\\techo -e "\\\\nFor shell, type \\\\e[1mkitten shell\\\\e[0m"\\n\\t\\techo -e "\\\\nFor help, \\\\e[1mkitten --help\\\\e[0m"\\n\\tend\\n\\tset -gx PATH "$PATH" "$HOME/.local/bin"\\nend' > /home/kitten/.config/fish/config.fish
      - chown kitten:kitten /home/kitten/.config/fish/config.fish
      # Install fisher to make it easy to install fish plugins.
      - su kitten -c 'curl -sL https://raw.githubusercontent.com/jorgebucaran/fisher/main/functions/fisher.fish | source && fisher install jorgebucaran/fisher'
      # Install fish plugins.
      # For details, see https://github.com/jorgebucaran/awsm.fish?tab=readme-ov-file#readme.
      - su kitten -c "fisher install nickeb96/puffer-fish"
      - su kitten -c "fisher install jethrokuan/z"
      - su kitten -c "fisher install IlanCosman/tide@v6 -y"
      - su kitten -c "tide configure --auto --style=Lean --prompt_colors='True color' --show_time='24-hour format' --lean_prompt_height='Two lines' --prompt_connection=Disconnected --prompt_spacing=Sparse --icons='Many icons' --transient=No"
      # Install Helix Editor
      - su kitten -c 'sudo dnf install helix -y'
      #
      # Install Kitten into the kitten account.
      #
      - wget -O/dev/null -q https://${callbackDomain}/domain/${token}/status/installing-kitten
      - su kitten -c 'wget -qO- https://codeberg.org/kitten/app/raw/install | bash'
      #
      # Deploy the app.
      #
      - wget -O/dev/null -q https://${callbackDomain}/domain/${token}/status/deploying-app
      - su kitten -c '/home/kitten/.local/bin/kitten deploy ${appGitHttpsUrl} --domain-token=${token} --small-web-host-domain=${callbackDomain} --is-being-deployed-by-domain'
      #
      # We’re done; notify the domain host that app is successfully deployed.
      #
      - wget -O/dev/null -q https://${callbackDomain}/domain/${token}/status/app-deployed

    final_message: "Welcome to your new 💕 Small Web place powered by Kitten."
  `
  return cloudConfig
}
