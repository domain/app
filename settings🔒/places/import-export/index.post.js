/**
  Receives uploaded places export.
*/
export default function ({ request }) {
  request.session.emit('placesExportUploaded', request.uploads[0])
}
