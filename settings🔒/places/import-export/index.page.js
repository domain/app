// @ts-check
import fs from 'node:fs/promises'
import kitten from '@small-web/kitten'
import { db } from '@app/database'

import Settings from '../../Settings.layout.js'
import SvgSpinner from './SvgSpinner.component.js'
import ok from '../../setup/ok.script.js'

export default class ImportAndExportPage extends kitten.Page {
  html () {
    const setupOk = ok()

    // Get list of registered domains.
    // TODO: Create an index for completed and non-completed domain setups
    // so we don’t have to do this every time.
    // TODO: Page this.
    // @ts-ignore proxy mixin methods
    const domains = Object.values(db.domains).filter(domain => domain.setup.complete && !domain.isPreWarmed)

    return kitten.html`
      <${Settings} activeSection='Import & Export'>
        <if ${setupOk}>
          <then>
            <markdown>
              ### Import

              Import the domain data you exported from a different Domain instance. The domains will be added to the existing list of places here. Duplicates will be discarded.

              > 💡 Both instances must be reachable from the same domain name. (This feature is meant to be used during development to move places between multiple computers all mapped to the same domain.)
            </markdown>
            <form
              id='import-form'
              hx-post='.'
              hx-encoding='multipart/form-data'
              hx-trigger='input from:#import'
              hx-swap='none'
            >
              <label for='import' class='prominent'>Domains to import (.json)</label>
              <div class='importControl'>
                <input
                  id='import'
                  name='import'
                  type='file'
                  accept='application/json'
                  connect
                  hx-trigger='input'
                  required
                ></input>
              </div>
            </form>
            <${ImportProgress.connectedTo(this)} />

            <if ${domains.length > 0}>
              <then>
                <markdown>
                  ### Export

                  Export the domain data for the places registered on this Domain instance as a JSON file.
                </markdown>
                <form method='GET' action='./export'>
                  <button type='submit'>Export domains</button>
                </form>
              </then>
            </if>
          </then>
          <else>
            <p>Your instance is not set up properly.</p>
            <p><strong>Please <a href='/settings/setup'>finish setting up</a> </strong></p>
          </else>
        </if>
      </>
    `
  }
}

class ImportProgress extends kitten.Component {
  importStarted = false
  importEnded = false
  message = 'Uploading…'
  /** @type {boolean|string} */
  error = false
  stats = {
    domainsFound: 0,
    domainsImported: 0,
    duplicatesIgnored: 0
  }

  html () {
    return kitten.html`
      <div id='import-status'>
        <if ${this.importStarted}>
          <if ${this.importEnded && !this.error}>
            <then>
              <markdown>
                #### Import complete!

                | Domains              |                                          |
                | -------------------- | ---------------------------------------- |
                | Found                | ${this.stats.domainsFound}               |
                | Imported             | ${this.stats.domainsImported}            |
                | Ignored (duplicates) | ${this.stats.duplicatesIgnored}          |
                
                [View places](/settings/places/)
              </markdown>
              <style>
                table {
                  table-layout: auto;
                }
              </style>
            </then>
            <else>
              <div><${SvgSpinner} /> ${this.message}</div>
            </else>
          </if>
        </if>
        <if ${this.error}>
          <div class='error'>❌ ${this.error}</div>
        </if>
      </div>
    `
  }

  onConnect () {
    this.addEventHandler(this.page.request.session, 'placesExportUploaded', /** @this {ImportProgress} */ async function (upload) {
      this.message = 'Processing…'
      this.update()

      // Ensure the upload looks like what we expect.
      if (upload.mimetype === 'application/json') {
        const file = await fs.readFile(upload.filePath, 'utf-8')

        let json
        try {
          json = JSON.parse(file)
        } catch (error) {
          this.error = `Could not parse file: ${error}`
        }

        // Ensure data is one of our exports.
        const requiredVersion = 1
        if (json.type === 'domains' && json.createdBy === 'Domain' && json.version === requiredVersion && json.domains !== undefined) {
          const domains = Object.keys(json.domains)
          if (domains.length > 0) {
            // OK, all seems good, let’s go ahead and import these domains
            // while excluding any duplicates.
            this.stats.domainsFound = 0
            this.stats.domainsImported = 0
            this.stats.duplicatesIgnored = 0

            domains.forEach(key => {
              this.stats.domainsFound++
              if (db.domains[key] !== undefined) {
                this.stats.duplicatesIgnored++
              } else {
                db.domains[key] = json.domains[key]
                this.stats.domainsImported++
              }
            })

            this.importEnded = true
          } else {
            this.error = `No domains found to import in file.`
          }
        } else {
          this.error = `File is not in required format. Please ensure this is an export from Domain. Also, ensure the version of the export is ${requiredVersion} to use the export with this version of Domain.`
        }
      } else {
        this.error = `Unexpected file type, cannot import (expected application/json, got ${upload.mimetype}).`
      }

      this.update()
    })
  }

  onImport () {
    this.importEnded = false
    this.importStarted = true
    this.update()
  }
} 
