/**
  Returns JSON file export of the places on this Domain.
*/
import kitten from '@small-web/kitten'
import db from '@app/database' 

/**
  @param {{ response: import('@small-web/kitten').KittenResponse }} parameterObject
*/
export default function ({ response }) {
  // Export all domains except those that are pre-warmed servers.
  const domains = Object.keys(db.domains).reduce((obj, key) => {
    const domain = db.domains[key]
    if (!domain.isPreWarmed && domain.setup.complete && !domain.setup.hasError && !domain.setup.cancelled) {
      obj[key] = db.domains[key]
    }
    return obj
  }, {})
  const domainsExport = {
    type: 'domains',
    createdBy: 'Domain',
    version: 1,
    date: new Date(),
    domains
  }
  const fileName = `domains-${kitten.domain}.json`
  response.jsonFile(domainsExport, fileName)
}
