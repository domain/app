// @ts-check
import kitten from '@small-web/kitten'
import { db } from '@app/database'

import PreWarmedDomains from '../../../PreWarmedDomains.script.js'
import Places from '../Places.layout.js'
import ok from '../../setup/ok.script.js'

// @ts-ignore CSS module.
import Styles from './Styles.fragment.css'

const MIN_NUMBER_OF_PREWARMED_DOMAINS = 0
const MAX_NUMBER_OF_PREWARMED_DOMAINS = 10

export default class PreWarmedDomainsPage extends kitten.Page {
  html () {
    const setupOk = ok()

    // Get list of registered domains.
    // TODO: Create an index for completed and non-completed domain setups
    // so we don’t have to do this every time.
    // TODO: Page this.
    // @ts-ignore proxy mixin methods
    const domains = Object.values(db.domains).filter(domain => domain.setup.complete && domain.isPreWarmed)

    return kitten.html`
      <${Places} activeSection='Pre-warmed'>
        <h2>Pre-warmed Domains</h2>

        <if ${setupOk}>
          <then>
            <p>This is the list of the pre-warmed domains that are deployed and running, waiting for a new Kitten app to be deployed on them from <a href='/settings/places/create'>Places → Create</a>.</p>
            <form
              name='updatePreWarmedDomains'
              confirm='Are you sure you want to update the number of pre-warmed domains to maintain? This will result in the creation or deletion of new servers.'
              connect
            >
              <label for='numberOfRequestedPreWarmedDomains'>Number of pre-warmed domains to maintain</label>
              <div id='compactControl'>
                <input
                  type='number'
                  id='numberOfRequestedPreWarmedDomains'
                  name='numberOfRequestedPreWarmedDomains'
                  min='${MIN_NUMBER_OF_PREWARMED_DOMAINS}'
                  max='${MAX_NUMBER_OF_PREWARMED_DOMAINS}'
                  required
                  value='${db.settings.vps.numberOfPreWarmedDomains}'
                >
                <button type='submit'>Update</button>
              </div>
            </form>

            <if ${domains.length > 0}>
              <then>
                <table>
                  <thead>
                    <tr>
                      <th scope='col'>Domain</th>
                      <th scope='col'>App</th>
                      <th scope='col'>State</th>
                      <th scope='col'>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    ${
                      domains.map(domain => kitten.html`
                        <tr>
                          <td>${domain.subdomain}</td>
                          <td><a href='${domain.application.source}' target='_blank' rel='noopener noreferrer'>${domain.application.name}</a></td>
                          <td>${domain.setup.complete && !domain.setup.hasError ? 'OK' : domain.setup.hasError ? domain.setup.error : domain.setup.state }</td>
                          <td>
                            <a
                              class='action'
                              href='https://${domain.subdomain}.${db.settings.dns.domain}'
                              target='_blank'
                              rel='noopener noreferrer'
                              inert=${!domain.setup.complete || domain.setup.hasError}
                            >View</a>
                            <a
                              class='action'
                              href='/settings/places/delete/${domain.subdomain}'
                              inert=${!domain.setup.complete || domain.setup.hasError}
                            >Delete</a>
                          </td>
                        </tr>
                      `)
                    }
                  </tbody>
                </table>
              </then>
              <else>
                <p><strong>None yet.</strong></p>
              </else>
          </then>
          <else>
            <p>Your instance is not set up properly.</p>
            <p><strong>Please <a href='/settings/setup'>finish setting up</a> </strong></p>
          </else>
        </if>
        <${Styles} />
      </>
    `
  }

  /**
    Person has requested to update the number of pre-warmed domains.

    @param {{ numberOfRequestedPreWarmedDomains: string }} data
  */
  onUpdatePreWarmedDomains (data) {
    const numberOfRequestedPreWarmedDomains = Number(data.numberOfRequestedPreWarmedDomains)
    console.log(numberOfRequestedPreWarmedDomains)
    if (!isNaN(numberOfRequestedPreWarmedDomains) && numberOfRequestedPreWarmedDomains >= MIN_NUMBER_OF_PREWARMED_DOMAINS && numberOfRequestedPreWarmedDomains <= MAX_NUMBER_OF_PREWARMED_DOMAINS) {
      PreWarmedDomains.getInstance().updateNumberOfRequestedPreWarmedDomains(numberOfRequestedPreWarmedDomains)
    }
  }
}
