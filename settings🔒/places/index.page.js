// @ts-check
import kitten from '@small-web/kitten'
import { db } from '@app/database'

import Places from './Places.layout.js'
import ok from '../setup/ok.script.js'

export default class PlacesPage extends kitten.Page {
  html () {
    const setupOk = ok()

    // Get list of registered domains.
    // TODO: Create an index for completed and non-completed domain setups
    // so we don’t have to do this every time.
    // TODO: Page this.
    // @ts-ignore proxy mixin methods
    const domains = Object.values(db.domains).filter(domain => domain.setup.complete && !domain.isPreWarmed)

    return kitten.html`
      <${Places}>
        <h2>Places</h2>

        <if ${setupOk}>
          <then>
            <p>This is the list of Small Web places that are currently being hosted by you.</p>

            <if ${domains.length > 0}>
              <then>
                <table>
                  <thead>
                    <tr>
                      <th scope='col'>Domain</th>
                      <th scope='col'>Application</th>
                      <th scope='col'>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    ${
                      domains.map((domain) => kitten.html`
                        <tr>
                          <td>${domain.subdomain}</td>
                          <td><a href='${domain.application.source}' target='_blank' rel='noopener noreferrer'>${domain.application.name}</a></td>
                          <td>
                            <a
                              class='action'
                              href='https://${domain.subdomain}.${db.settings.dns.domain}'
                              target='_blank'
                              rel='noopener noreferrer'
                            >View</a>
                            <a
                              class='action'
                              href='/settings/places/delete/${domain.subdomain}'
                            >Delete</a>
                          </td>
                        </tr>
                      `)
                    }
                  </tbody>
                </table>
              </then>
              <else>
                <p><strong>Nothing yet.</strong></p>
                <p>Why not <a href='/settings/places/create'>create one</a>?</p>
              </else>
          </then>
          <else>
            <p>Your instance is not set up properly.</p>
            <p><strong>Please <a href='/settings/setup'>finish setting up</a> </strong></p>
          </else>
        </if>
      </>
    `
  }
}

