<img src='./domain-logo.svg' alt='Domain logo: a kitten sleeping in front of her tiny house while holding onto its green yarn of wool.' style='margin-left: auto; margin-right: auto; display: block; width: 7em;'>

# Domain

__Work-in-progress (not ready for use).__

> *Currently being re-written from scratch in [Kitten](https://codeberg.org/kitten/app).*

Domain is a free and open [Small Web](https://small-tech.org/research-and-development/) hosting platform.

(Think of it as a batteries-included Digital Ocean in-a-box for [Kitten](https://codeberg.org/kitten/app)-based apps.

> For an in-depth description of Domain, please see our [NLnet Grant Application for Domain](https://ar.al/2022/07/29/nlnet-grant-application-for-domain/) (which was rejected, yay!) Also see: [#16 - Update concept of “application” - app - Codeberg.org](https://codeberg.org/domain/app/issues/16).

## For organisations to serve individuals

Organisations can use Domain to provide Small Web hosting services at their own Small Web domains to their communities.

These services can be:

- A commercial activity (e.g., our not-for-profit Small Technology Foundation is planning on charging ~€10/month for small web places on small-web.org in order to fund ongoing development of Kitten, Domain, and our other work.)

- For the public good (e.g., a municipality could provide free Small Web places to its citizens via tokens. ([Small Technology Foundation](https://small-tech.org) actually prototyped such a system with the City of Ghent a few years ago.)

- A *hyperlocal* or more personal initiative (e.g., providing Small Web places for your neighbourhood or your own family via a private instance).

Individuals can use Small Web hosts to set up their own Small Web places at Small Web domains without any technical knowledge.

> 💡 See [legal matters concerning Small Web hosts](#legal-matters-concerning-small-web-hosts).

## Small Web Domain

A Small Web domain is, for all intents and purposes, like a top-level domain but:

1. It is a second-level domain (e.g., small-web.org),
2. Where Small Web places are registered as subdomains,
3. And, unless it is being run as a private instance (e.g., for your family), where the second-level domain is registered on the [Public Suffix List](https://publicsuffix.org/).

If a person wants to move their place to a different Small Web domain or to a Big Web domain (i.e., their own second-level domain), they should be able to do so at any time.

## Apps

A Small Web domain enables you to install apps written for [Kitten](https://codeberg.org/kitten/app).

By limiting to scope to apps written in Kitten we enable the following properties:

- **Generation of a secret** that only the person who owns the site being commissioned knows, which they can use both within the app they’re installing and to administer their hosting relationship with the Domain instance (e.g., to cancel their hosting, etc.)

- **Integrated domain administration functionality** in the Small Web app via server-to-server communication between the Small Web host and the person’s Small Web place.

## Cryptographical properties

Please see the [cryptographical properties section in the Kitten documentation](https://codeberg.org/kitten/app#user-content-cryptographical-properties).

## Install

1. Install [Kitten](https://codeberg.org/kitten/app)
2. Clone this repo.

## Run

Domain is a [Kitten](https://codeberg.org/kitten/app) app. There is no build stage.

From the Domain folder, simply run:

```shell
kitten
```

- Main page: https://localhost
- Settings Panel: https://localhost/settings

🚧 For the Settings Panel password please see terminal output on first run. Please store this secret in your password manager.

## Configure

Once you sign up for accounts with the [supported service providers](#supported-service-providers) and Domain is running, you can configure your account using the Admin Panel.

## Is this ready for use?

No. It is under heavy development.

## Why such a generic name?

On purpose. We are not trying to create A Brand™ here. The “brand”, if anything, is the instantly-recognisable simplicity and typographic treatment of the public-facing sign-up page itself which will enable people to recognise Small Web domains (due to the lack of bullshit/noise). Otherwise, the focus is on the domain itself, as it should be.

## What does it take to run a Small Web Domain?

Domain integrates three* main services:

1. A Domain Name Service (DNS) provider for setting up domains.

2. A Virtual Private Server (VPS) provider for settings up servers.

3. A payment provider to limit access to scarce resources.

_\* For public instances, it also requires that your domain is registered on the [Public Suffix List](https://publicsuffix.org/)._

## Supported service providers

Domain provides support for the following service providers. You will need to get accounts with them to configure it.

### DNS

- [DNSimple](https://dnsimple.com)

### VPS

- [Hetzner Cloud](https://www.hetzner.com/cloud)

The VPS setup is handled via cloud-init. To debug any issues you might experience with the server setup, the following commands (executed on the server) should help:

```shell
cloud-init analyze dump
systemctl status cloud-final.service
cat /var/log/cloud-init-output.log
```

### Payment

- __None:__ for private instances (e.g., families, internal use at organisations, etc.)

- __Tokens:__ for use in place of money by organisations running public instances (e.g., a city providing free Small Web Places to its citizens). __(Under construction.)__

- __[Stripe](https://stripe.com):__ for commercial payments (e.g., for making your Small Web Domain financially sustainable within the capitalist system you find yourself living under) __(Under construction.)__

## Legal matters concerning Small Web hosts

__None of the information in this section should be considered legal advice. Please consult a lawyer before setting up your Small Web host.__

As a Small Web host, you will have certain legal obligations.

Specifically, you must comply with:

- The terms and conditions of your [DNS provider](#dns)
- The terms and conditions of your [VPS provider](#vps)
- The terms and conditions of your [Payment provider](#payment) (if any)
- Relevant local laws

In Europe (and in hosts servicing European citizens), this includes:

- [General Data Protection Regulation (GDPR)](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32016R0679)
- [Digital Services Act (DSA)](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32022R2065)

To make this easier for you, Domain automatically generates terms and conditions and a privacy policy for you that we believe are compliant with the DSA (for micro and small businesses; i.e., any business that has less than 25 employees and a turnaround of less than €10M) and GDPR.

__You should have all legal contracts reviewed by your lawyer.__

___Small Technology Foundation accepts no liability for any legal costs or action arising from using the automatically-generated documents. Based on the advice of your lawyers, you can include your own manually-generated documents, either based on the automatically-generated ones or otherwise, in place of the automatically-generated ones.___

### On the GDPR

While Domain itself does not gather any personal information directly, the service providers that you use do.

So your privacy policy must reflect the privacy policy/terms/data protection agreement (if any) of your:

  - [DNS provider](#dns)
  - [VPS provider](#vps)
  - [Payment provider](#payment) (if any)

The automatically-generated privacy policy does this by default.

More information on the GDPR implications of each provider is available from the provider’s page in Settings → Setup.

### On the applicability of the Digital Services Act (DSA)

Domain is Small Web hosting software designed for use by:

  - individuals
  - internally by organisations
  - micro and small businesses

The first two use cases do not fall under the jurisdiction of the Digital Services Act (DSA) as these are for personal and internal use and no provision of intermediary services is being performed.

The last use case falls under [Article 6 (Hosting)](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32022R2065#d1e2134-1-1) of the DSA. The relevant sections of the DSA that require action from you to comply with are the following:

>#### [Article 11: Points of contact for Member States’ authorities, the Commission and the Board](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32022R2065#d1e2393-1-1)
>
>1.   Providers of intermediary services shall designate a single point of contact to enable them to communicate directly, by electronic means, with Member States’ authorities, the Commission and the Board referred to in Article 61 for the application of this Regulation.
>
>2.   Providers of intermediary services shall make public the information necessary to easily identify and communicate with their single points of contact. That information shall be easily accessible, and shall be kept up to date.
>
>3.   Providers of intermediary services shall specify in the information referred to in paragraph 2 the official language or languages of the Member States which, in addition to a language broadly understood by the largest possible number of Union citizens, can be used to communicate with their points of contact, and which shall include at least one of the official languages of the Member State in which the provider of intermediary services has its main establishment or where its legal representative resides or is established.

In the automatically-generated terms and conditions, the organisation’s support email address is used as the point of contact.

> #### [Article 12: Points of contact for recipients of the service](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32022R2065#d1e2413-1-1)
>
>1.   Providers of intermediary services shall designate a single point of contact to enable recipients of the service to communicate directly and rapidly with them, by electronic means and in a user-friendly manner, including by allowing recipients of the service to choose the means of communication, which shall not solely rely on automated tools.
>
>2.   In addition to the obligations provided under Directive 2000/31/EC, providers of intermediary services shall make public the information necessary for the recipients of the service in order to easily identify and communicate with their single points of contact. That information shall be easily accessible, and shall be kept up to date.

In the automatically-generated terms and conditions, the organisation’s support email address is used as the point of contact.

> 💡If you are not based in the EU, you will also have to comply with [Article 13: Legal representatives.](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32022R2065#d1e2428-1-1) There does not appear to be a micro and small business exemption for this rule.

> #### [Article 14: Terms and conditions](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32022R2065#d1e2458-1-1)
>
> 1.   Providers of intermediary services shall include information on any restrictions that they impose in relation to the use of their service in respect of information provided by the recipients of the service, in their terms and conditions. That information shall include information on any policies, procedures, measures and tools used for the purpose of content moderation, including algorithmic decision-making and human review, as well as the rules of procedure of their internal complaint handling system. It shall be set out in clear, plain, intelligible, user-friendly and unambiguous language, and shall be publicly available in an easily accessible and machine-readable format.
> 
> 2.   Providers of intermediary services shall inform the recipients of the service of any significant change to the terms and conditions.
> 
> …
> 
> 4.   Providers of intermediary services shall act in a diligent, objective and proportionate manner in applying and enforcing the restrictions referred to in paragraph 1, with due regard to the rights and legitimate interests of all parties involved, including the fundamental rights of the recipients of the service, such as the freedom of expression, freedom and pluralism of the media, and other fundamental rights and freedoms as enshrined in the Charter.
> 
> …

The automatically-generated terms and conditions (or your manually-entered terms and conditions) satisfy this requirement.

(Points 3, 5, and 6 do not apply in general to Domain instances. If your instance is primarily directed at minors, you will be affected by point 3.)

> 💡 As a micro or small business, you are exempt from [Article 15 (Transparency reporting obligations for providers of intermediary services)](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32022R2065#d1e2493-1-1)

All of [Section 2](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32022R2065#d1e2546-1-1) applies to micro and small businesses, this includes:

- [Article 16: Notice and action mechanisms](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32022R2065#d1e2556-1-1)
- [Article 17: Statement of reasons](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32022R2065#d1e2617-1-1)
- [Article 18: Notification of suspicions of criminal offences](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32022R2065#d1e2713-1-1)

Article 16 is also handled by the support email address you configured under Settings → Setup → Organisation.

Article 17 can be satisfied via a number of ways:

  - 🚧 Issuing a notice to the server via Domain that is then displayed in place of the site.
  - Shutting down the instance and communicating this via the email address you have from the payment provider.

That the hosting service is bound by Article 18 should be mentioned in the terms and conditions (and it is in the automatically-generated terms and conditions).

## What is the token payment type?

The Small Web aims to be a bridge between the capitalist centralised web and a post-capitalist humanscale web of individually-owned-and-controlled single-tenant web places. As such, we have the unenviable task of trying to design a system that is both sustainable under capitalism and viable for post-capitalist use.

Supporting both Stripe and tokens as a payment type is an example of this.

A token is simply a secret code that you can enter in place of traditional payment with money.

For example, a municipality might decide that its citizens having their own place on the Small Web is good for human rights and democracy and might budget to provide them with this service from the common purse, for the common good. As such, it might create codes that get mailed out to all citizens. They can then use these codes in place of payment. (We prototyped an early version of this with the City of Ghent several years ago. Unfortunately, a conservative government came into power and our funding for the project was cut off.)

Traditional/token payment doesn’t have to be mutually exclusive. The municipality in question might, for example, enable both so that people can sign up for more than one Small Web place or if it wants to enable others (e.g., people who are not residents of their city) to sign up.

## Project conventions

  - HTML `id` attributes use kebap-case (all lowercase). Justification: `/because-when-you-use/#camelCaseInURLsItsInconsistentAndHardToTypeIn`
  - HTML `name` attributes use camelCase. Justification: can be used on the server by JavaScript, e.g., in POST requests (consistency).
  - HTML `class` attributes use camelCase, with component names in PascalCase. Justification: consistency with JavaScript; ability to differentiate components (what BEM would call “blocks”, even though we don’t use BEM, we do have components)
  - Single quotes for HTML attributes are preferred. (By all means, use double quotes if you need to include single quotes in the value.)

## Like this? Fund us!

[Small Technology Foundation](https://small-tech.org) is a tiny, independent not-for-profit.

We exist in part thanks to patronage by people like you. If you share [our vision](https://small-tech.org/about/#small-technology) and want to support our work, please [become a patron or donate to us](https://small-tech.org/fund-us) today and help us continue to exist.

## LICENSE

Copyright ⓒ 2021-present [Aral Balkan](https://ar.al), [Small Technology Foundation](https://small-tech.org)

Released under AGPL version 3.0.
