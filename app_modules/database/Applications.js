/**
  Applications.

  The list of Kitten applications that can be installed from this Domain instance.
*/

// @ts-check

import Application from './Application.js'

export default class Applications {
  title = 'Applications'

  list = [
    new Application ({
      name: 'Place',
      source: 'https://codeberg.org/place/app.git'
    }),
    new Application ({
      name: 'Domain',
      source: 'https://codeberg.org/domain/app.git'
    })
  ]

  // TODO: Is setupAttempted really required for applications?
  setupAttempted = false

  // The constructor is called by JSDB with a parameter object
  // when deserialising persisted objects.
  constructor (parameters = {}) {
    Object.assign(this, parameters)
  }

  get ok () {
    // There should be at least one app.
    return this.list.length > 0
  }
}
