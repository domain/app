/**
  Payment provider: Tokens.

  Payment provider where tokens are exchanged for Small Web places.
*/

// @ts-check

export default class PaymentProviderTokens {
  static id = 'tokens'
  
  name = 'Tokens'
  generatedTokens = []
  claimedTokens = []

  // The constructor is called by JSDB with a parameter object
  // when deserialising persisted objects.
  constructor (parameters = {}) {
    Object.assign(this, parameters)
  }

  get ok () {
    return this.generatedTokens.length > 0 && this.claimedTokens.length < this.generatedTokens.length
  }
}
