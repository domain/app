/**
  DNS details.
*/

// @ts-check

export default class DNS {
  title = 'Domain Name Service (DNS)'
  setupAttempted = false
  ok = false
  domain = ''
  accountId = 0
  accessToken = ''
  error = ''

  // The constructor is called by JSDB with a parameter object
  // when deserialising persisted objects.
  constructor (parameters = {}) {
    Object.assign(this, parameters)
  }
}
