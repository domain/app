/**
  Application.

  Represents a Kitten application.
*/

// @ts-check

export default class Application {
  /**
    Create an application with a name and a URL to a git repository (source).
  */
  constructor ({ name, source }) {
    this.name = name
    this.source = source
  }
}
