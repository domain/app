/**
  Organisation details.
*/

// @ts-check

export default class Organisation {
  title = 'Organisation'
  name = ''
  address = ''
  site = ''
  email = ''
  setupAttempted = false
  error = null

  // The constructor is called by JSDB with a parameter object
  // when deserialising persisted objects.
  constructor (parameters = {}) {
    Object.assign(this, parameters)
  }

  get ok () {
    return this.name !== ''
      && this.address !== ''
      && this.site !== ''
      && this.email !== ''
  }
}
