/**
  Payment provider: Paddle.

  Paddle is a commercial payment provider (Merchant of Record) that can accept
  payments in fiat currency via credit card and other means of payment (e.g., Apple Pay).

  As a Merchant of Record (MoR), Paddle sells subscriptions for you, collects the payment,
  accounts for and remits taxes globally, etc. You get payments on the 1st and 15th of the
  month that you have to pay your regular corporation tax on.

  https://www.paddle.com/
*/

// @ts-check

import PaddleEnvironments from './PaddleEnvironments.js'

export default class PaymentProviderPaddle {
  static id = 'paddle'

  name = 'Paddle'
  environment = new PaddleEnvironments()

  // The constructor is called by JSDB with a parameter object
  // when deserialising persisted objects.
  constructor (parameters = {}) {
    Object.assign(this, parameters)
  }

  get ok () {
    return this.environment.ok
  }
}
