/**
  Payment.

  Root of all payment-related data.
*/

// @ts-check

import PaymentProviderNone from './PaymentProviderNone.js'
import PaymentProviderPaddle from './PaymentProviderPaddle.js'
import PaymentProviderTokens from './PaymentProviderTokens.js'

export default class Payment {
  title = 'Payment'
  setupAttempted = false
  error = null
  selectedProvider = PaymentProviderNone.id

  // Note: we could use something link [PaymentProviderNone.id]: … here
  // but we’re using static strings instead so we get better autocompletion
  // during authoring.
  providerNames = ['none', 'tokens', 'paddle']

  // Note: we could use something link [PaymentProviderNone.id]: … here
  // but we’re using static strings instead so we get better autocompletion
  // during authoring.
  providers = {
    'none': new PaymentProviderNone(),
    'tokens': new PaymentProviderTokens(),
    'paddle': new PaymentProviderPaddle()
  }

  // The constructor is called by JSDB with a parameter object
  // when deserialising persisted objects.
  constructor (parameters = {}) {
    Object.assign(this, parameters)
  }

  get ok () {
    return this.providers[this.selectedProvider].ok
  }
}
