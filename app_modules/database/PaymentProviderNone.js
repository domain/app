/**
  PaymentProviderNone

  The default payment provider (for private instances, where payments are not used
  and all domain setups are done from the settings interface).
*/

// @ts-check

export default class PaymentProviderNone {
  static id = 'none'
  name = 'None'
  ok = true

  // The constructor is called by JSDB with a parameter object
  // when deserialising persisted objects.
  constructor (parameters = {}) {
    Object.assign(this, parameters)
  }
}
