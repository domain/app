/**
  Paddle Environments

  Paddle has two environments: sandbox and live.

  The sandbox is for testing (no actual transactions take place). After you’ve
  sure your instance works in the sandbox, you can set it live to take actual payments.
*/

// @ts-check

import PaddleEnvironment from "./PaddleEnvironment.js"

export default class PaddleEnvironments {
  current = 'sandbox'
  sandbox = new PaddleEnvironment({ type: PaddleEnvironment.SANDBOX })
  live = new PaddleEnvironment({ type: PaddleEnvironment.LIVE })

  // The constructor is called by JSDB with a parameter object
  // when deserialising persisted objects.
  constructor (parameters = {}) {
    Object.assign(this, parameters)
  }

  get setupAttempted () {
    return this.sandbox.setupAttempted && this.live.setupAttempted
  }

  get ok () {
    return this.sandbox.ok && this.live.ok
  }
}
