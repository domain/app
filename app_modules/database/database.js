/**
  Database.

  Strongly-typed database module that wraps Kitten’s untyped global db instance.
  (See https://codeberg.org/kitten/app#using-javascript-database-jsdb-a-not-so-scary-database)

  The initialise() method is a hook that’s automatically called by Kitten when starting the server.
  It should not be called manually from within Domain.
*/

//@ts-check

import path from 'node:path'
import JSDB from '@small-tech/jsdb'

import DNS from './DNS.js'
import VPS from './VPS.js'
import PSL from './PSL.js'
// @ts-ignore File name casing warning.
import Settings from './Settings.js'
import Organisation from './Organisation.js'
import Application from './Application.js'
import Applications from './Applications.js'
import Payment from './Payment.js'
import PaymentProvider from './PaymentProvider.js'
import PaymentProviderNone from './PaymentProviderNone.js'
import PaymentProviderTokens from './PaymentProviderTokens.js'
import PaymentProviderPaddle from './PaymentProviderPaddle.js'
import PaddleEnvironment from './PaddleEnvironment.js'
import PaddleEnvironments from './PaddleEnvironments.js'
import { RegistrationFormState } from '../../settings🔒/places/create/RegistrationForm.component.js'

/**
  Metadata about the database itself.

  This is inlined here instead of being kept in its own Database.js file like all
  the other model classes so as not to leave our friends who have Appleⓡ Macⓡ
  devices that cannot differentiate between database.js (this file) and Database.js
  (that file). Perhaps when Apple becomes a Gazillion billion-dollar company…
*/

class Database {
  initialised = false

  // The constructor is called by JSDB with a parameter object
  // when deserialising persisted objects.
  constructor (parameters = {}) {
    Object.assign(this, parameters)
  }
}

// Export all database-related classes for better ergonomics during authoring.
export {
  DNS,
  VPS,
  PSL,
  Database,
  Settings,
  Organisation,
  Application,
  Applications,
  Payment,
  PaymentProvider,
  PaymentProviderNone,
  PaymentProviderTokens,
  PaymentProviderPaddle,
  PaddleEnvironment,
  PaddleEnvironments,
  RegistrationFormState
}

// This app module wraps the global Kitten db (JSDB database) instance
// and adds type definitions (the schema used in Domain) to it
// before exporting it for import from routes that want to make
// use of strict type checking during development.

/**
  // A single domain record.
  @typedef {{
    subdomain: string,
    domain: string,
    isPreWarmed: boolean,
    dns?: {
      id: Number,
      deleted?: Boolean,
    },
    setup: {
      complete: boolean,
      hasError: boolean,
      error?: string,
      cancelled: boolean,
      state: string,
      startedAt?: Date,
      endedAt?: Date
    },
    server: {
      id?: Number,
      createdAt?: Date,
      token: string,
      deleted?: Boolean,
      ipv4?: string,
      ipv6?: string
    },
    application: {
      name: string,
      source: string,
    }
  }} Domain

  // Database schema.
  @typedef {object} DatabaseSchema

    // Database table.
    @property {Database} database

    // Domains table.
    @property { Object.<string, Domain>} domains

    // Pre-warmed domains are servers that have already been created
    // and are running Kitten, ready for a Small Web app to be deployed on them.
    @property { Array<string> } preWarmedDomains

    // Tokens to Domains table.
    @property { Object.<string, string> } tokensToDomains

    // Settings table.
    @property {Settings} settings

    // Sessions table.
    @property { Object.<string, {
      registrationFormState?: RegistrationFormState
    }>} sessions
*/

/** Domain’s JSDB database. */
// Note: the jsdbCompactOnLoad environment variable is set
// by Kitten’s database commands that require compact on load to be turned off
// so that they do no alter the structure of tables if the app is running
// (and being debugged during development, for example, via the db tail command).
// We have to adhere to this too in Database App Modules.
const compactOnLoad = process.env.jsdbCompactOnLoad === 'false' ? false : true
export const db = /** @type {DatabaseSchema} */ (
  JSDB.open(
    path.join(globalThis.kitten.paths.APP_DATA_DIRECTORY, 'db'),
    {
      compactOnLoad, 
      classes: [
        DNS,
        VPS,
        PSL,
        Database,
        Settings,
        Organisation,
        Application,
        Applications,
        Payment,
        PaymentProvider,
        PaymentProviderNone,
        PaymentProviderTokens,
        PaymentProviderPaddle,
        PaddleEnvironment,
        PaddleEnvironments,
        RegistrationFormState
      ]
    }
  )
)

export async function initialise () {
  if (!db.database) {
    db.database = new Database() 
  }

  if (!db.database.initialised) {
    // List of registered domains.
    // @ts-ignore internal JSDB interface.
    if (db.domains) { await db.domains.__table__.delete() }
    db.domains = {}

    // List of pre-warmed domains.
    // @ts-ignore internal JSDB interface.
    if (db.preWarmedDomains) { await db.preWarmedDomains.__table__.delete() }
    db.preWarmedDomains = []

    // Map of domain tokens to domains.
    // @ts-ignore internal JSDB interface.
    if (db.tokensToDomains) { await db.tokensToDomains.__table__.delete() }
    db.tokensToDomains = {}

    // Default settings.
    // @ts-ignore internal JSDB interface.
    if (db.settings) { await db.settings.__table__?.delete() }
    db.settings = new Settings()

    // A copy of the list of sessions kept by Kitten
    // in our own database so we can store custom objects
    // (interface state objects) in it.
    // See https://codeberg.org/kitten/app#sessions
    // @ts-ignore internal JSDB interface.
    if (db.sessions) { await db.sessions.__table__?.delete() }
    db.sessions = {}
    
    db.database.initialised = true

    console.debug(`\n  • Database initialised.`)
  }

  // Even if the database is initialised, check for required objects
  // and create them if they don’t exist. This will, for example,
  // allow objects added to later versions of the database schema to
  // work when upgrading.

  if (db.domains === undefined) db.domains = {}
  if (db.preWarmedDomains === undefined) db.preWarmedDomains = []
  if (db.tokensToDomains === undefined) db.tokensToDomains = {}
  if (db.settings === undefined) db.settings = new Settings()
  if (db.settings.vps.numberOfPreWarmedDomains === undefined) db.settings.vps.numberOfPreWarmedDomains = 1
  if (db.sessions === undefined) db.sessions = {}

  return db
}

export default db
