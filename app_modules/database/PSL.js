/**
  PSL (Public Suffix List)

  Persists whether or not the domain is on the Public Suffix List.
  https://publicsuffix.org/
*/

// @ts-check

export default class PSL {
  title = 'Public Suffix List (PSL)'
  setupAttempted = false
  ok = true // Since default payment type is ‘none’
            // PSL is also valid as it’s not a requirement
            // for that payment type.

  // The constructor is called by JSDB with a parameter object
  // when deserialising persisted objects.
  constructor (parameters = {}) {
    Object.assign(this, parameters)
  }
}
