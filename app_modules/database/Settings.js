/**
  The Settings table.
*/

// @ts-check

import Applications from './Applications.js'
import DNS from './DNS.js'
import Organisation from './Organisation.js'
import PSL from './PSL.js'
import Payment from './Payment.js'
import VPS from './VPS.js'

export default class Settings {
  organisation = new Organisation()
  dns = new DNS()
  vps = new VPS()
  payment = new Payment()
  psl = new PSL()
  applications = new Applications()

  // The constructor is called by JSDB with a parameter object
  // when deserialising persisted objects.
  constructor (parameters = {}) {
    Object.assign(this, parameters)
  }

  get ok () {
    return this.organisation.ok
      && this.dns.ok
      && this.vps.ok
      && this.payment.ok
      && this.psl.ok
      && this.applications.ok
  }
}
