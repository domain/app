/**
  VPS details.
*/

// @ts-check

export default class VPS {
  title = 'Virtual Private Server (VPS)'
  setupAttempted = false
  ok = false
  error = null
  apiToken = ''
  numberOfPreWarmedDomains = 1
  sshKeyName = 'None (recommended)'
  serverTypeName = 'cpx11'
  locationName = 'fsn1'
  imageName = 'alma-9'

  // The constructor is called by JSDB with a parameter object
  // when deserialising persisted objects.
  constructor (parameters = {}) {
    Object.assign(this, parameters)
  }
}
