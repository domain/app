/**
  Paddle Environment.

  Contains data necessary for Domain to use a Paddle environment for payments.
*/

// @ts-check

/**
  @typedef {{
    success: boolean,
    response?: Array.<Object>,
    error: {
      code: number,
      message: string
    }
  }} PaddleResponse
*/
/** @typedef {'sandbox'} Sandbox */
/** @typedef {'live'} Live */

export default class PaddleEnvironment {
  /** @type Sandbox */ static SANDBOX = 'sandbox'
  /** @type Live */ static LIVE = 'live'

  static baseUrl = {
    'sandbox': 'https://sandbox-vendors.paddle.com/api/2.0',
    'live': 'https://vendors.paddle.com/api/2.0'
  }
  
  vendorId = ''
  vendorAuthCode = ''
  planId = ''
  setupAttempted = false
  ok = false
  error = ''
  /** @type {(Sandbox|Live)} */
  type = PaddleEnvironment.SANDBOX
  
  // The constructor is called by JSDB with a parameter object
  // when deserialising persisted objects.
  /**
    @param {{
      vendorId?: typeof PaddleEnvironment.prototype.vendorId,
      vendorAuthCode?: typeof PaddleEnvironment.prototype.vendorAuthCode,
      planId?: typeof PaddleEnvironment.prototype.planId,
      setupAttempted?: typeof PaddleEnvironment.prototype.setupAttempted,
      ok?: typeof PaddleEnvironment.prototype.ok,
      error?: typeof PaddleEnvironment.prototype.error,
      type?: typeof PaddleEnvironment.prototype.type,
    }?} parameters
  */
  constructor (parameters = {}) {
    Object.assign(this, parameters)
  }

  /**
    Persists passed parameters after validating them via the Paddle API.
  
    @param {{ vendorId: string, vendorAuthCode: string, planId: string }} parameters
    @return {Promise<PaddleResponse>}
  */
  async update({ vendorId, vendorAuthCode, planId }) {
    this.setupAttempted = true
    this.ok = false

    // Persist the values to begin with, even if they’re wrong,
    // so they can be presented back to the person on the client
    // to allow them to edit them.
    this.vendorId = vendorId
    this.vendorAuthCode = vendorAuthCode
    this.planId = planId

    const url = `${PaddleEnvironment.baseUrl[this.type]}/subscription/plans`

    // Validate the Paddle parameters.
    
    const paddleParameters = {
      vendor_id: Number(vendorId),
      vendor_auth_code: vendorAuthCode,
      plan: Number(planId)
    }

    const _response = await fetch(url, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(paddleParameters)
    })
    const response = await _response.json()

    if (!response.success) {
      this.error = response.error.code === 107
        ? `API credentials are incorrect.` 
        : `Paddle error ${response.error.code}: ${response.error.message}`

      return response
    }

    // Make sure the plan id exists because the Paddle API does not throw
    // an error if it does not.
    let planIdExists = false
    response.response?.forEach(plan => {
      if (plan.id === Number(planId)) {
        planIdExists = true
      }
    })

    if (!planIdExists) {
      this.error = `Invalid Subscription Plan ID.`

      return response
    }

    this.ok = true
    return response
  }
}
