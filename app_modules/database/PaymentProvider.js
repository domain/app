/**
  PaymentProvider interface.
*/

// @ts-check

export default class PaymentProvider {
  name = ''
  ok = false

  // The constructor is called by JSDB with a parameter object
  // when deserialising persisted objects.
  constructor (parameters = {}) {
    Object.assign(this, parameters)
  }
}

