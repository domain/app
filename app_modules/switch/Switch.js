//@ts-check
import { html } from '@small-web/kitten'

const toCamelCase = (/** @type string */ string) => string
  .replace(/[^a-zA-Z ]/g, '')                                                  // Remove non-alphabetical characters apart from spaces (e.g., parentheses, etc.)
  .split(' ')                                                                  // Split on words.
  .map(                                                                        // Start each word apart from the first with a capital letter.
    (/** @type string */ word, /** @type number */ index) =>
      index === 0 ? word.toLowerCase() : word[0].toUpperCase() + word.slice(1)
  ) 
  .join('')                                                                    // Join back into a single string without spaces.

/**
  Switch component.

  Port of Adam Argyle’s switch component.
  (https://github.com/argyleink/gui-challenges/tree/main/switch)

  Note: the slot is ignored.

  @param {{ name: string, label: string, checked: boolean, SLOT: object }} props
*/
export default function ({ name, label='Missing label', checked = false, SLOT, ...props }) {
  // If a form element name is not provided, set it as the camelCase version of the label.
  if (name === undefined) name = toCamelCase(label)

  // @ts-ignore
return html`
    <label for'switch' class='Switch'>
      ${label}
      <input name='${name}' type='checkbox' role='switch' checked=${checked} ...${props}>
    </label>

    <style>
      .Switch {
        --isLTR: 1;
        --track-size: calc(var(--thumb-size) * 2);
        --track-padding: 2px;

        --track-inactive: hsl(80 0% 80%);
        --track-active: hsl(80 60% 45%);

        --track-color-inactive: var(--track-inactive);
        --track-color-active: var(--track-active);

        --thumb-size: 2rem;
        --thumb: hsl(0 0% 100%);
        --thumb-highlight: hsl(0 0% 0% / 25%);

        --thumb-color: var(--thumb);
        --thumb-color-highlight: var(--thumb-highlight);

        --thumb-transition-duration: 0.33s;

        display: flex;
        align-items: center;
        gap: 2ch;
        justify-content: space-between;

        cursor: pointer;
        user-select: none;
        -webkit-tap-highlight-color: transparent;
      }

      .Switch:dir(rtl) {
        --isLTR: -1;
      }

      .Switch input {
        /* Reset margin in case a default was set for form inputs. */
        margin: inherit;
      }

      @media (prefers-color-scheme: dark) {
        .Switch {
          --track-inactive: hsl(80 0% 35%);
          --track-active: hsl(80 60% 60%);
          --thumb: hsl(0 0% 5%);
          --thumb-highlight: hsl(0 0% 100% / 25%);
        }
      }
    
      .Switch > input {
        --thumb-position: 0%;

        appearance: none;
        border: none;
        outline-offset: 5px;
        box-sizing: content-box;

        padding: var(--track-padding);
        inline-size: var(--track-size);
        block-size: var(--thumb-size);
        background: var(--track-color-inactive);
        border-radius: var(--track-size);

        flex-shrink: 0;
        display: grid;
        align-items: center;
        grid: [track] 1fr / [track] 1fr;
      }

      .Switch > input::before {
        content: "";
        grid-area: track;
        inline-size: var(--thumb-size);
        block-size: var(--thumb-size);

        background: var(--thumb-color);
        border-radius: 50%;

        box-shadow: 0 0 0 var(--highlight-size) var(--thumb-color-highlight);

        transform: translateX(var(--thumb-position));
      }

      @media (prefers-reduced-motion: no-preference) {
        .Switch > input::before {
          transition: 
            transform var(--thumb-transition-duration) ease,
            box-shadow .25s ease;
        }
      }

      .Switch > input:not(:disabled):hover::before {
        --highlight-size: .5rem;
      }

      /* Positioned at the end of the track: track length - 100% (thumb width). */
      .Switch > input:checked {
        background: var(--track-color-active);
        --thumb-position: calc((var(--track-size) - 100%) * var(--isLTR));
      }

      /* Positioned in the center of the track: half the track - half the thumb. */
      .Switch > input:indeterminate {
        --thumb-position: calc(
          ((var(--track-size) / 2) - (var(--thumb-size) / 2))
          * var(--isLTR)
        );
      }
    </style>
  `
}
