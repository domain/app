export default function (request, socket) {
  console.log(`Domain setup: new host socket connection for ${request.params.token}`)

  socket.addEventListener('message', event => {
    console.log(`${request.params.token}: ${event}`)
  })
}
