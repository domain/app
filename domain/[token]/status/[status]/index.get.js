// @ts-check

import { events } from '@small-web/kitten'

/**
  Emit any event received from a remote server that’s being set up.
  The event name is the secret token and the only argument is the
  name of the status update.
*/
export default function ({ request, response }) {
  const token = request.params.token
  const status = request.params.status
  console.log('Domain token:', token)
  console.log('Domain status:', status)
  if (token !== undefined && status !== undefined) {
    events.emit(token, status)
  }
}
