// @ts-check
/**
  Handles domain registration and place setup.
*/

import { EventEmitter } from 'node:events'
import HetznerCloud from 'hcloud-js'
import { DNSimple } from 'dnsimple'
import kitten from '@small-web/kitten'
import db from '@app/database' 
import PreWarmedDomains from './PreWarmedDomains.script.js'

export default class Registrar extends EventEmitter {
  static events = {
    start: Symbol('start'),
    warning: Symbol('warning'),
    error: Symbol('error'),
    progress: Symbol('progress'),
    success: Symbol('success')
  }

  /** @type Registrar */
  static instance

  static getInstance () {
    if (this.instance === undefined) {
      this.instance = new this()
    }
    return this.instance
  }

  /**
    Register a domain and set up a place (Kitten instance and app) at it.

    @param {{ subdomain: string, applicationName:string, gitHttpsCloneUrl: string }} parameterObject
  */
  async register ({ subdomain, applicationName, gitHttpsCloneUrl }) {
    // TODO: Implement error checking for custom source to ensure it is
    // a valid git URL and can be cloned.

    // Find the next available pre-warmed domain.
    const preWarmedDomains = PreWarmedDomains.getInstance()
    const preWarmedDomain = preWarmedDomains.getNextAvailablePreWarmedDomain()
    if (preWarmedDomain === null) {
      // TODO: Handle this better with retry.
      this.emit(subdomain, { type: Registrar.events.error, message: 'No pre-warmed domains available.' })
      return
    }

    // Set the per-warmed domain’s setup status back to incomplete
    // as we’re updating it, so it will show up in the troubleshooting
    // section if anything goes wrong with the update.
    preWarmedDomain.setup.complete = false

    // Also mark it as no longer pre-warmed as it is in the process
    // of being converted into a regular domain.
    preWarmedDomain.isPreWarmed = false

    this.emit(subdomain, { type: Registrar.events.start })

    // As soon as possible, let’s kick off the DNS record creation
    // (and deletion of the old one.) We don’t edit the existing record
    // as that would be much slower (even with a TTL of 60, as we currently
    // have the records set up with).
    // TODO: Modified from PreWarmedDomains.script.js; check for redundancy
    // and refactor if necessary.
  
    const dnsHost = new DNSimple({
      accessToken: db.settings.dns.accessToken
    })

    const ipv4 = preWarmedDomain.server.ipv4
    // const _ipv6 = preWarmedDomain.server.ipv6 // currently unused. TODO: look into this.

    // Save a copy of the current DNS record ID so we can delete it later.
    const oldDNSRecordId = preWarmedDomain?.dns?.id
  
    // page.send(status(`About to create new DNS record for ${subdomain}`))

    // Create an A record for the subdomain that points to the server’s IP address.
    // TODO: ALso create AAAA record for the ipv6
    // Create zone record
    let dnsZoneCreationResponse
    try {
      dnsZoneCreationResponse = await dnsHost.zones.createZoneRecord(
        /* accountId */ db.settings.dns.accountId,
        /* zoneId */ db.settings.dns.domain,
        /* attributes */ {name: subdomain, type: 'A', content: ipv4, ttl: 60}
      )
    } catch (error) {
      console.error('DNS error', error)
      preWarmedDomain.setup.endedAt = new Date()
      preWarmedDomain.setup.hasError = true
      preWarmedDomain.setup.error = error.message
      preWarmedDomain.setup.state = 'failed-while-setting-up-dns'

      // TODO: Handle this better with retry.
      this.emit(subdomain, { type: Registrar.events.error, message: `Could not update DNS: ${error}` })
      return
    }

    this.emit(subdomain, { type: Registrar.events.progress, message: 'Domain registered.' })

    // Persist zone record id (so we can use it to delete it if we need to later).
    preWarmedDomain.dns = {
      id: dnsZoneCreationResponse.data.id
    }

    // Now that DNS has been taken care of, let’s start on updating the VPS server.

    const domainToken = preWarmedDomain.server.token
    const fullDomain = `${subdomain}.${db.settings.dns.domain}`
  
    // Now that we have the secret token, we can also calculate and the secret
    // one-time-use URL on the place being created where the person can set up the
    // secret/identity for their place.
    // const secretUrl = `https://${fullDomain}/💕/hello/${domainToken}/`
    // page.send(status(`Secret URL: ${secretUrl}`))

    // Create the POST URL for communicating with the pre-warmed domain.
    const deploymentUrl = `https://${preWarmedDomain.subdomain}.${db.settings.dns.domain}/💕/deploy/`

    // console.log(`Deployment URL: ${deploymentUrl}`)

    // Set up listener for the server-ready event that will be sent to us
    // once the Kitten server has restarted. Once we receive this, we
    // can finish off the registration process.
    const serverSetupStatusHandler = async setupStatus => {
      if (setupStatus === 'server-ready') {
        // page.send(status('Server has restarted.'))

        // This is the last status we expect from the CloudInit script. We
        // can remove the listener now so we don’t leak memory.
        kitten.events.removeListener(domainToken, serverSetupStatusHandler)

        // The server is ready so let’s make the first request (to trigger
        // the Let’s Encrypt TLS certificate privisioning). Once this
        // call successfully returns, we can be sure that the server can
        // be accessed immediately by the person so we can tell them that
        // it’s ready.
        // page.send(status('Checking for domain availability and resulting in TLS certificates being created.'))

        this.emit(subdomain, { type: Registrar.events.progress, message: 'Provisioning security certificate…'})

        // The /💕/ok/ GET route is a well-known route that we know will work on any successful
        // Kitten deployment. All it does is return a 200 Success result.
        const url = `https://${fullDomain}/💕/ok/`
        let response
        try {
          response = await fetch(url)
        } catch (error) {
          preWarmedDomain.setup.endedAt = new Date()
          preWarmedDomain.setup.error = error.message
          preWarmedDomain.setup.state = 'fetch-error-while-attempting-to-reach-deployed-place'

          this.emit(subdomain, { type: Registrar.events.error, message: `Fetch failed while attempting to reach deployed place: ${preWarmedDomain.setup.error}` })
          return
        }

        // Also try and fetch the index page of the deployed site
        // and report if we get an error to let the person know that their
        // app has an error (so that if they hit the index page and get
        // an error, they don’t think it was the deployment that failed).
        // NOTE: This does NOT abort/roll back the deployment.
        const appIndexUrl = 'https://${fullDomain}/'
        try {
          const appIndexResponse = await fetch(appIndexUrl)
          if (appIndexResponse.status >= 400) {
            this.emit(subdomain, { type: Registrar.events.warning, message: `App was deployed successfully but attempt to access index page returned error (code ${appIndexResponse.status}).`})
          }
        } catch (error) {
          this.emit(subdomain, { type: Registrar.events.warning, message: `App was deployed successfully but attempt to access index page failed: ${error.message}.`})
        }

        preWarmedDomain.setup.endedAt = new Date()
        if (response.ok) {
          // Update the pre-warmed domain’s record (and set its
          // state so it no longer shows as a pre-warmed domain
          // but a regular registered domain.
          const preWarmedDomainSubdomain = preWarmedDomain.subdomain
          preWarmedDomain.application.name = applicationName
          preWarmedDomain.application.source = gitHttpsCloneUrl
          preWarmedDomain.subdomain = subdomain
          preWarmedDomain.setup.state = 'place-is-reachable'
          preWarmedDomain.setup.complete = true

          // And update the state of the database.
          db.domains[subdomain] = preWarmedDomain

          this.emit(subdomain, { type: Registrar.events.success, domainToken })

          // Housekeeping.

          // (These tasks do not concern the person setting up the Small Web place, they’re
          // for our sake – to clean up the VPS and DNS records – so we carry them out after
          // the person’s site has been set up to keep that process as fast as possible.)

          // Now that the new DNS record has been created, let’s delete the old one.

          try {
            if (oldDNSRecordId === undefined) throw new Error('Old DNS record ID is undefined.')
            await dnsHost.zones.deleteZoneRecord(
              /* accountId */ db.settings.dns.accountId,
              /* zoneId */ db.settings.dns.domain,
              /* recordId */ oldDNSRecordId
            )
          } catch (error) {
            console.warn(`Warning: Could not delete old DNS record for pre-warmed domain: ${error}`)
          }

          // To make it easier for us to manage servers in Hetzner, let’s
          // go ahead an rename the server there to match the domain it is
          // now running on. 

          try {
            const webHost = new HetznerCloud.Client({
              token: db.settings.vps.apiToken,
              timeout: 30000 // 30 seconds
            })
            await webHost.servers.changeName(preWarmedDomain.server.id, fullDomain)
          } catch (error) {
            console.warn(`Warning: Could not change name of VPS server with ID ${preWarmedDomain.server.id} to ${fullDomain}: ${error}`)
          }

          // Finally, delete the old copy of the pre-warmed domain subdomain.
          delete db.domains[preWarmedDomainSubdomain]
        } else {
          //
          // Response from call to deployed domain’s /💕/ok/ route did not
          // fail with an error but it also was not 200 OK.
          //
          // So the deployment failed.
          //
          preWarmedDomain.setup.hasError = true
          const responseText = await response.text()
          preWarmedDomain.setup.error = `${response.status} ${response.statusText}: ${responseText}`
          preWarmedDomain.setup.state = 'failed-while-attempting-to-reach-deployed-place'
          this.emit(subdomain, { type: Registrar.events.error, message: `Deployment failed while attempting to reach deployed place: ${response.status} ${response.statusText}: ${responseText}`})
        }
      }
    }

    // Listen for events from the server we’re setting up.
    // The handler, above, is where is the process we are starting below will be completed.
    // (Note divergence of code and chrological flows.)
    kitten.events.addListener(domainToken, serverSetupStatusHandler)

    this.emit(subdomain, { type: Registrar.events.progress, message: 'Deploying app…' })

    try {
      // Instruct the Kitten server to carry out the deployment.
      await fetch(deploymentUrl, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          gitHttpsCloneUrl,
          domainToken,
          domain: fullDomain
        }) 
      })
      this.emit(subdomain, { type: Registrar.events.progress, message: 'Starting server…' })
      // console.log(`Kitten app from ${gitHttpsCloneUrl} deployed`)
    } catch (error) {
      this.emit(subdomain, { type: Registrar.events.error, message: `Deployment failed: ${error}` })
    }
  }
}
