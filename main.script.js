// @ts-check

/**
  Domain – entrypoint.

  The main function gets called by Kitten after all project modules
  have been installed but before routes are initialised and before
  Kitten starts serving the app.

  It is a good place if you need to set up any globals, intialise
  things, set up long-running processes, and/or display initial output
  for your app.
*/

import fs from 'node:fs'
import path from 'node:path'
import db from '@app/database'
import PreWarmedDomains from './PreWarmedDomains.script.js'

// FIXME: Add type definition for Kitten app to Kitten globals
// (https://codeberg.org/kitten/globals/src/branch/main/globals.js)
export default function main (_app) {
  displayHeader()
  ensureRequestedNumberOfPreWarmedDomains()
  listenForUpdatesToSettingsState()
  displayFooter()
}

/**
  Indents string to line up with rest of the text in the Kitten command-line interface.

  @param {string} string
*/
function indent (string) {
  return '   ' + string
}

/**
  Makes text boldface using ANSI sequence.

  @param {string} string
*/
function bold (string) {
  const start = '\x1b['
  const end = 'm'
  const bold = 1
  const normal = 0
  return `${start}${bold}${end}${string}${start}${normal}${end}`
}

function line () {
  console.info(indent('─'.repeat(60)))
}

function displayHeader() {
  if (process.env.basePath == undefined) throw new Error('Required basePath environment variable is undefined.')

  const version = JSON.parse(fs.readFileSync(path.join(process.env.basePath, 'package.json'), 'utf-8')).version
  line()
  console.info()
  console.info(bold(`📍 Domain version ${version}`))
  console.info(indent('https://codeberg.org/domain/app'))
}

const preWarmedDomains = PreWarmedDomains.getInstance()
function ensureRequestedNumberOfPreWarmedDomains () {
  preWarmedDomains.createOrDeletePreWarmedDomainsAsNecessary()
}

// Save initial state of the settings table.
// For use in listenForUpdatesToSettingsState(), below.
let settingsWasOK = db.settings.ok 

/**
  Listen for changes on the settings table so we can trigger checks for
  pre-warmed domains when settings becomes OK after not being OK.
*/
function listenForUpdatesToSettingsState () {
  console.info('🙉 Listening for state of Settings.')
  // @ts-ignore JSDB proxy property.
  db.settings.__table__.addListener('persist', () => {
    const settingsIsOK = db.settings.ok
    if (settingsWasOK && !settingsIsOK) {
      // Settings was OK but is no longer OK. Mark that
      // so we know to check for pre-warmed domains next time
      // it becomes OK.
      console.log('Settings was OK but became not OK. Will check for pre-warmed domain next time it becomes OK again.')
      settingsWasOK = false
    } else if (!settingsWasOK && settingsIsOK) {
      // Settings was not OK but is it now OK, let’s
      // check for pre-warmed domains.
      console.log('Settings has become OK, about to check for pre-warmed domains')
      settingsWasOK = true
      preWarmedDomains.createOrDeletePreWarmedDomainsAsNecessary()
    } else {
      // Otherwise (if settings was ok and is still ok or if
      // settings was not OK and is still not OK, we don’t need
      // to do anything).
      console.log('Settings is still', settingsWasOK ? 'ok' : 'not ok', 'ignoring…')
    }
  })
}

function displayFooter () {
  console.info()
  line()
  console.info()
}
