// @ts-check
import { html } from '@small-web/kitten'

// @ts-ignore CSS module.
import Styles from './Footer.fragment.css'

export default () => html`
  <footer class='Footer'>
    <markdown>
      This is a [Small Web](/about) host powered by [Domain](https://codeberg.org/domain/app). Own this domain? [Sign in](/settings).

      Domain &copy; 2022-present by [Aral Balkan](https://ar.al) is free software licensed under [AGPL Version 3.0](https://www.gnu.org/licenses/agpl-3.0.en.html).

      Domain is brought to you with &hearts; by [Small Technology Foundation](https://small-tech.org)

      __Like this? [Fund the Small Web!](https://small-tech.org/fund-us)__
    </markdown>
    <${Styles} />
  </footer>
`
