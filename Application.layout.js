// @ts-check
import { html, PAGE } from '@small-web/kitten'
import Footer from './Footer.component.js'

// @ts-ignore CSS module.
import ApplicationStyles from './Application.fragment.css'
// @ts-ignore CSS module.
import SettingsStyles from './Settings.fragment.css'

/**
  Main application layout.
*/
export default ({ layout, SLOT, ...props }) => html`
  <page htmx css>
  <div id='application' ...${props}>
    ${SLOT}
  </div>

  <content for=${PAGE.head}>
    <meta name='description' content='Domain is a free and open Small Web hosting platform.'>
  </content>

  <${Footer} />
  <${ApplicationStyles} />
  <if ${layout==='settings'}>
    <${SettingsStyles} />
  </if>
`
